// SPDX-License-Identifier: GPL-2.0+
/*
 * (C) Copyright 2002
 * Richard Jones, rjones@nexus-tech.net
 */

/*
 * Boot support
 */
#include <common.h>
#include <command.h>
#include <rpi_local_cmn.h>
#include <stdlib.h>
#include <malloc.h>

/* コマンド用関数定義 */
	/* 初期化関数 */
INT_32 do_rpi_lcmn_init(cmd_tbl_t *cmdtp, INT_32 flag, INT_32 argc, INT_8 * const argv[])
{
	/* 初期化 */
	if( argc != 1 )
	{
		printf("do_rpi_lcmn_init cmd NG argc=%d\n",argc);
		return CMD_RET_USAGE;
	}
	/* 初期化 */
	rpi_lcmn_init();
	
	printf("do_rpi_lcmn_init OK\n");
	return CMD_RET_SUCCESS;
}
	/* ログレベル関数 */
INT_32 do_rpi_lcmn_log_lvl(cmd_tbl_t *cmdtp, INT_32 flag, INT_32 argc, INT_8 * const argv[])
{
	ULONG32      ret_log_lvl = 0;
	E_RET_DEFINE ret         = E_RET_OK ;
	
	if (argc != 2)
	{
		printf("do_rpi_lcmn_log_lvl cmd NG argc=%d\n",argc);
		return CMD_RET_USAGE;
	}

	ret_log_lvl = simple_strtoul(argv[1], NULL, 16);
	
	ret = rpi_lcmn_lvl_chg((UINT_32)ret_log_lvl);
	if( ret != E_RET_OK )
	{
		printf("do_rpi_lcmn_log_lvl ret=%d\n",ret);
		ret = CMD_RET_FAILURE;
	}
	printf("do_rpi_lcmn_log_lvl OK\n");
	return ret;
}
	/* ログダンプ関数 */
INT_32 do_rpi_lcmn_log_FromRamToConsole(cmd_tbl_t *cmdtp, INT_32 flag, INT_32 argc, INT_8 * const argv[])
{
	E_RET_DEFINE ret = E_RET_OK ;
	
	/* ログ出力テスト用コード Start */
#if 0
	// 関数内部のログ数:62byte+ユーザ定義のデバック文字列数:11+16+164+1[\n]= 192[NULL含むと193]byte 計:62+192 =254[NULL入れると255文字]byte
	D_TP(D_CMN_LOG_LVL_FATAL,"TEST FATAL=%d.%d.%d.%d.%d.%d.%d.%d 01234567890123456789012345678901234567890123456789012345678901234567890123456789 01234567890123456789012345678901234567890123456789012345678901234567890123456789012\n"   ,D_CMN_LOG_LVL_FATAL,1,2,3,4,5,6,7);
	// 関数内部のログ数:62byte+ユーザ定義のデバック文字列数:11+16+165+1[\n]= 193[NULL含むと194]byte 計:62+193 =255[NULL入れると256文字で超過]byte
	D_TP(D_CMN_LOG_LVL_FATAL,"TEST FATAL=%d.%d.%d.%d.%d.%d.%d.%d 01234567890123456789012345678901234567890123456789012345678901234567890123456789 012345678901234567890123456789012345678901234567890123456789012345678901234567890123\n"  ,D_CMN_LOG_LVL_FATAL,1,2,3,4,5,6,7);
	// 関数内部のログ数:62byte+ユーザ定義のデバック文字列数:11+16+166+1[\n]= 194[NULL含むと195]byte 計:62+194 =256[NULL入れると257文字で超過]byte
	D_TP(D_CMN_LOG_LVL_FATAL,"TEST FATAL=%d.%d.%d.%d.%d.%d.%d.%d 01234567890123456789012345678901234567890123456789012345678901234567890123456789 0123456789012345678901234567890123456789012345678901234567890123456789012345678901234\n" ,D_CMN_LOG_LVL_FATAL,1,2,3,4,5,6,7);
	D_TP(D_CMN_LOG_LVL_ERROR,"TEST ERROR=%d\n",D_CMN_LOG_LVL_ERROR);
	D_TP(D_CMN_LOG_LVL_WARN ,"TEST WARN =%d\n",D_CMN_LOG_LVL_WARN );
	D_TP(D_CMN_LOG_LVL_INFO ,"TEST INFO =%d\n",D_CMN_LOG_LVL_INFO );
	D_TP(D_CMN_LOG_LVL_DEBUG,"TEST DEBUG=%d\n",D_CMN_LOG_LVL_DEBUG);
	D_TP(D_CMN_LOG_LVL_TRACE,"TEST TRACE=%d\n",D_CMN_LOG_LVL_TRACE);
#endif
	/* ログ出力テスト用コード End */
	
	if( argc != 1 )
	{
		printf("do_rpi_lcmn_log_FromRamToConsole cmd NG argc=%d\n",argc);
		return CMD_RET_USAGE;
	}
	ret = rpi_lcmn_log_FromRamToConsole();
	if( ret != E_RET_OK )
	{
		printf("do_rpi_lcmn_log_FromRamToConsole ret=%d\n",ret);
		ret = CMD_RET_FAILURE;
	}
	printf("do_rpi_lcmn_log_FromRamToConsole OK\n");
	return CMD_RET_SUCCESS;
}
	/* ファイル読み込み */
	/* rpi_lcmn_FileRead 0x01000000 100 */
INT_32 do_rpi_lcmn_FileRead(cmd_tbl_t *cmdtp, INT_32 flag, INT_32 argc, INT_8 * const argv[])
{
	E_RET_DEFINE ret    = E_RET_OK ;
	INT_32  cnt         = 0;
	INT_32  fs_ret      = 0;
	ULONG32 addr        = 0;
	loff_t  Offset      = 0;
	loff_t  OffsetLen   = 0;
	INT_8   *dev_name   = NULL;
	INT_8   *dev_part_str   = NULL;
	INT_8   *FileName   = NULL;
//	INT_8   FileName_w[64] = "/boot/file_wfile_4.txt";
//	UINT_8  str[0x100]  = { 0 };
	UINT_32 read_len    = 0; 
	/* コマンドラインデバック Start */
	for( cnt = 0 ; cnt < argc ; cnt++ )
	{
		D_TP( D_CMN_LOG_LVL_DEBUG , "argv[%d]=%s\n", cnt , (INT_8 *)argv[cnt] );
	}
	/* コマンドラインデバック End   */
	addr    = simple_strtoul(argv[1], NULL, 16);
//	len     = simple_strtoul(argv[2], NULL, 16);
	Offset  = (loff_t)simple_strtoul(argv[2], NULL, 16);
	OffsetLen =(loff_t) simple_strtoul(argv[3], NULL, 16);
	
	dev_name = argv[4];
	dev_part_str = argv[5];
	FileName = argv[6];
	D_TP( D_CMN_LOG_LVL_DEBUG , "addr:0x%lx\n", addr );
	D_TP( D_CMN_LOG_LVL_DEBUG , "Offset :0x%x\n"  , (UINT_32)Offset );/* loff_t型は64bit型なので、表示おかしくなるのでキャストして表示する */
	D_TP( D_CMN_LOG_LVL_DEBUG , "OffsetLen :%d\n" , (UINT_32)OffsetLen );/* loff_t型は64bit型なので、表示おかしくなるのでキャストして表示する */
	D_TP( D_CMN_LOG_LVL_DEBUG , "dev_name :%s %s\n"  , dev_name , dev_part_str );
	D_TP( D_CMN_LOG_LVL_DEBUG , "FileName :%s\n"  , FileName );
	
#if 1
	fs_ret = rpi_lcmn_fat_fsread( 
				addr ,               /* addr							:システムメモリ内のアドレス       */
				Offset,              /* Offset :file_addless postion   :ファイルの相対アドレス位置        */
				OffsetLen,           /* OffsetLen                      :ファイルの相対アドレス位置の長さ  */
				dev_name ,           /* dev_name                       :U-bootのデバイス名                */
				dev_part_str ,       /* dev_part_str                   :U-bootのパーツ文字列              */
				FileName ,           /* filename                       :ファイル名[ディレクトリ付き]      */
				&read_len );         
#endif
	
#if 0 // ファイル->構造体コピー処理テスト
	fs_ret = rpi_lcmn_fat_fsreadStr( 
				addr ,               /* addr							:システムメモリ内のアドレス       */
				Offset,              /* Offset :file_addless postion   :ファイルの相対アドレス位置        */
				OffsetLen,           /* OffsetLen                      :ファイルの相対アドレス位置の長さ  */
				dev_name ,           /* dev_name                       :U-bootのデバイス名                */
				dev_part_str ,       /* dev_part_str                   :U-bootのパーツ文字列              */
				FileName ,           /* filename                       :ファイル名[ディレクトリ付き]      */
				&read_len ,
		 		&str[0]);            /* 読み出し先の構造体 */
#endif
	if( fs_ret != 0 )
	{
		D_TP( D_CMN_LOG_LVL_ERROR , "Error 0x%x\n",fs_ret );
		return CMD_RET_FAILURE;
	}
#if 0 // ファイル->構造体コピー処理テスト
	for( cnt = 0 ; cnt < 0x100 ; cnt++ )
	{
		printf("str[%d]=%x\n",cnt,str[cnt]);
	}
#endif
	printf("read_len=%d\n",read_len);
#if 0 //ファイルシステム書き込みテスト	
	fs_ret = rpi_lcmn_fat_fswrite( 
		addr ,
		(UINT_32)OffsetLen , 
		dev_name , 
		dev_part_str , 
		&FileName_w );
	
	if( fs_ret != 0 )
	{
		D_TP( D_CMN_LOG_LVL_ERROR , "Error 2 0x%x\n",fs_ret );
		return CMD_RET_FAILURE;
	}
#endif
	/* ファイルの変数読み出し方法の確認 */
	/* コピー先:変数 コピー元:アドレス 長さ  */
	//memcpy( &str , (unsigned char *)addr , len );
	//for( cnt = 0 ; cnt < 0x20 ; cnt++ )
	//{
	//	printf("str: %x\n",str[cnt]);
	//}
	
	
	return ret;
}
	/* 対象ディレクトリのファイル名、サイズを表示 */
	/* rpi_lcmn_GetFileNameAndSize /boot */
INT_32  do_rpi_lcmn_GetFileNameAndSize(cmd_tbl_t *cmdtp, INT_32 flag, INT_32 argc, INT_8 * const argv[])
{
	E_RET_DEFINE ret    = E_RET_OK ;
	INT_32  cnt         = 0;
	UINT_32  lm_ret     = 0;
	INT_8  *DirName     = NULL;
	INT_8  FileName[64] = { 0 };
	UINT_32 fsize       = 0;
	
	/* コマンドラインデバック Start */
	for( cnt = 0 ; cnt < argc ; cnt++ )
	{
		D_TP( D_CMN_LOG_LVL_DEBUG , "argv[%d]=%s\n", cnt , (INT_8 *)argv[cnt] );
	}
	/* コマンドラインデバック End   */
	DirName = argv[1];

	D_TP( D_CMN_LOG_LVL_DEBUG , "DirName:%s\n", DirName );

	/* LMファイルNAMEGet処理 */
	lm_ret = rpi_lcmn_FmtFileNameGet(	DirName                                   ,
										( INT_8 * )&D_CMN_FILE_LM_FMT_NAME        ,
										( UINT_32 )strlen(D_CMN_FILE_LM_FMT_NAME) ,
										( INT_8 * )&D_CMN_FILE_LM_FMT_EXT         ,
										( UINT_32 )strlen(D_CMN_FILE_LM_FMT_EXT)  ,
										"mmc",
										"0:1",
										( INT_8 * )&FileName );         
	if( lm_ret != 0 )
	{
		D_TP( D_CMN_LOG_LVL_ERROR , "Error 0x%x\n",lm_ret );
		return CMD_RET_FAILURE;
	}
	printf("FileName=%s\n",FileName);
	lm_ret = rpi_lcmn_FileSizeGet(	DirName  ,
									"mmc"    ,
									"0:1"    ,
									FileName ,
									&fsize );
	if( lm_ret != 0 )
	{
		D_TP( D_CMN_LOG_LVL_ERROR , "rpi_lcmn_FileSizeGet Error fsize=%d\n",fsize );
		return CMD_RET_FAILURE;
	}
	printf("fsize=%d\n",fsize);
	return ret;
}
INT_32 do_rpi_lcmn_LogFileWrite(cmd_tbl_t *cmdtp, INT_32 flag, INT_32 argc, INT_8 * const argv[])
{
	E_RET_DEFINE ret    = E_RET_OK ;
	INT_32  cnt         = 0;
	E_RET_DEFINE  fs_ret= 0;
	INT_8   *dev_name   = NULL;
	INT_8   *dev_part_str   = NULL;
	INT_8   *FileName   = NULL;
	/* コマンドラインデバック Start */
	for( cnt = 0 ; cnt < argc ; cnt++ )
	{
		D_TP( D_CMN_LOG_LVL_DEBUG , "argv[%d]=%s\n", cnt , (INT_8 *)argv[cnt] );
	}
	/* コマンドラインデバック End   */
	dev_name = argv[1];
	dev_part_str = argv[2];
	FileName = argv[3];
	D_TP( D_CMN_LOG_LVL_DEBUG , "dev_name :%s %s\n"  , dev_name , dev_part_str );
	D_TP( D_CMN_LOG_LVL_DEBUG , "FileName :%s\n"  , FileName );
	
	fs_ret = rpi_lcmn_log_FromRamToFile( 
		dev_name , 
		dev_part_str ,  
		FileName );
	if( fs_ret != 0 )
	{
		D_TP( D_CMN_LOG_LVL_ERROR , "Error 2 0x%x\n",fs_ret );
		return CMD_RET_FAILURE;
	}
	return ret;
}
/* 以下コマンド定義 */
	/* 初期化コマンド */
U_BOOT_CMD(
	rpi_init,			/* コマンド */
	1,                  /* 最大引数の数 */
	0,                  /* 繰り返し数   */
	do_rpi_lcmn_init,   /* コマンド実行用の関数 */
	"rpi init",         /* 使用方法 */
	"initialize\n"      /* help コマンド:-1の時表示される */
);
	/* ログレベル変更コマンド */
U_BOOT_CMD(
	rpi_lcmn_log_lvl,             /* コマンド */
	2,                            /* 最大引数の数 */
	0,                            /* 繰り返し数   */
	do_rpi_lcmn_log_lvl,          /* コマンド実行用の関数 */
	"rpi log level",              /* 使用方法 */
	" rpi_lcmn_log_lvl number \n" /* help コマンド:-1の時表示される */
	" FATAL 01 \n"
	" ERROR 02 \n"
	" WARN  04 \n"
	" INFO  08 \n"
	" DEBUG 16 \n"
	" TRACE 32 "
);
	/* ログ出力コマンド */
U_BOOT_CMD(
	rpi_lcmn_log_dump,               /* コマンド */
	1,                               /* 最大引数の数 */
	0,                               /* 繰り返し数   */
	do_rpi_lcmn_log_FromRamToConsole,/* コマンド実行用の関数 */
	"rpi log dump",                  /* 使用方法 */
	" rpi_lcmn_log_dump "            /* help コマンド:-1の時表示される */
);
	/* ファイル読み込みコマンド */
U_BOOT_CMD(
	rpi_lcmn_FileRead,			 /* コマンド */
	8,                           /* 最大引数の数 */
	0,                           /* 繰り返し数   */
	do_rpi_lcmn_FileRead,        /* コマンド実行用の関数 */
	"rpi_lcmn_FileRead",         /* 使用方法 */
	"File Read\n"                /* help コマンド:-1の時表示される */
);
	/* ファイルシステムからファイル名とサイズの読み込みコマンド */
U_BOOT_CMD(
	rpi_lcmn_GetFileNameAndSize,			 /* コマンド */
	2,                           /* 最大引数の数 */
	0,                           /* 繰り返し数   */
	do_rpi_lcmn_GetFileNameAndSize,        /* コマンド実行用の関数 */
	"rpi_lcmn_GetFileNameAndSize",         /* 使用方法 */
	"File Read Get NameAndSize\n"                /* help コマンド:-1の時表示される */
);
	/* ファイルシステムからファイル名とサイズの読み込みコマンド */
U_BOOT_CMD(
	rpi_lcmn_LFW,				 /* コマンド */
	4,                           /* 最大引数の数 */
	0,                           /* 繰り返し数   */
	do_rpi_lcmn_LogFileWrite,   /* コマンド実行用の関数 */
	"rpi_lcmn_LogFileWrite",    /* 使用方法 */
	"File Write logFile\n"      /* help コマンド:-1の時表示される */
);
//U_BOOT_CMD(
//	rpi_init,			/* コマンド */
//	1,                  /* 最大引数の数 */
//	0,                  /* 繰り返し数   */
//	do_rpi_lcmn_init,   /* コマンド実行用の関数 */
//	"rpi init",         /* 使用方法 */
//	"initialize\n"      /* help コマンド:-1の時表示される */
//);
