/*  */
/*
 * ラズベリーパイ専用
 * 
 */

#ifndef _RPI_LOCAL_H_
#define _RPI_LOCAL_H_

#include <common.h>
#include <stdio.h>
#include <stdarg.h>
#include <configs/rpi.h>
#include <asm/io.h>
#include <mapmem.h>
#include <fs.h>
#include <fat.h>
#include <linux/math64.h>

#include <env.h>
#include <env_internal.h>
#include <search.h>
#include <malloc.h>

/**型宣言 Start**/
typedef void                   VOID  ;
typedef char                   INT_8 ;
typedef short                  INT_16;
typedef int                    INT_32;
typedef long long int          INT_64;

typedef unsigned char          UINT_8 ;
typedef unsigned short         UINT_16;
typedef unsigned int           UINT_32;
typedef unsigned long          ULONG32;
typedef unsigned long long int UINT_64;
typedef size_t                  SIZE_T;
/**型宣言 End  **/

/**define Start**/
	/* 汎用機能 Start */
#define D_CMN_ON   1
#define D_CMN_OFF  0
	/* 汎用機能 End   */
	/* ログ機能 Start*/
	/* メモリに格納[性能とか,UART周りのデバックに良いかも] */
	/* Traceにレベルを設ける */
	/* レベル仕様 */
	/*情報量は最上位が最小、最下位が最大となる。
	   最上位
		↑FATAL 致命的なエラー。プログラムの異常終了を伴うようなもの。コンソール等に即時出力することを想定
		｜ERROR エラー。予期しないその他の実行時エラー。コンソール等に即時出力することを想定
		｜WARN  警告。廃要素となったAPIの使用、APIの不適切な使用、エラーに近い事象など、実行時に生じた異常とは言い切れないが正常とも異なる何らかの予期しない問題。コンソール等に即時出力することを想定
		｜INFO  情報。実行時の何らかの注目すべき事象（開始や終了など）。コンソール等に即時出力することを想定。従ってメッセージ内容は簡潔に止めるべき
		｜DEBUG デバッグ用の情報。システムの動作状況に関する詳細な情報。コンソールではなくログ上にだけ出力することを想定
		↓TRACE トレース情報。更に詳細な情報。コンソールではなくログ上にだけ出力することを想定 
	   最下位
	   defaultのログレベルはWARN
	   TODO:機能指定でデバックのON/OFFしたい
	   実現方法は以下
	   1.グローバル変数を分ける →グローバル変数は配列化が望ましいか? 1byte配列のサイズでエンディアンを意識せずにpointerでインクリメントしてbit探索してON/OFFチェック
	   2.グローバル変数の値はbit単位で機能を分ける。
	   3.各bitは機能を示し,bit:ONは表示、OFFは非表示
	   4.ログレベルの表示と機能ごとのデバックON/OFFは機能分ける
	   →関数化とマクロが必要？
	     方針として,引数を追加したログ機能を作る。
	     cmn_buf_printfのラッピングして、ログの有無を判定をする関数を作成する。
	     構造体で機能ごとをディレクトリと考える/ファイルごとと考える/関数単位ごとと考える。
	     →fmtからディレクトリ名抜き出し/ファイル名抜き出し/関数名抜き出しをしてbuf格納の有無をチェック後出力ON/OFFする。
	     ファイルに特殊な構造体宣言をして指定マクロが定義されていたら、出力とかも簡易的かも...
	     
	 */
#define D_CMN_LOG_LVL_FATAL 0x01
#define D_CMN_LOG_LVL_ERROR 0x02
#define D_CMN_LOG_LVL_WARN  0x04
#define D_CMN_LOG_LVL_INFO  0x08
#define D_CMN_LOG_LVL_DEBUG 0x10
#define D_CMN_LOG_LVL_TRACE 0x20

#define D_CMN_LOG_MICRO_TIME  1000000
#define D_CMN_LOG_MILLI_TIME  1000
#define D_CMN_LOG_SECOND_TIME 1000000
#define D_CMN_LOG_1MIN_TIME   60
#define D_CMN_LOG_1HOUR_TIME  60
#define D_CMN_LOG_1DAY_TIME   24 /* 未使用 */

#define D_CMN_DEV_MMC            "mmc" /* SDカードのデバイス名 */
#define D_CMN_DEV_MMC_PART       "0:1" /* SDカードのデバイスパーテション名 */
#define D_CMN_LOG_FILE_PATH      "/bootlog"
#define D_CMN_LOG_FILE_NAME      "/bootlog/RPI_UBOOT.LOG"
#define D_CMN_LOG_FILE_NAME_ONLY "RPI_UBOOT.LOG"
#define D_CMN_LOG_FILE_TMP_ADDR  0x01000000 /* ファイル→RAMへ一時的に使用する領域の開始位置 */
#define D_CMN_LOG_INIT_STR       "<uboot-log:0>\n" /* ログ初期化文字列 */
#define D_CMN_LOG_FMT_CHK_BEF    "<uboot-log:"     /* ログフォーマット前用チェック文字列 */
#define D_CMN_LOG_FMT_CHK_AFT    ">\n"             /* ログフォーマット後用チェック文字 */

#define D_CMN_LOG_NEWLINE_CHK    "\n"              /* 改行チェック */

	/* ログ機能 End*/
	/* ファイル関連機能 Start */
#define D_CMN_FILE_LM_FMT_PATH   "/LM"		/* LMのディレクトリ */
#define D_CMN_FILE_LM_FMT_NAME   "RPI_LM_F"	/* LMフォーマット名 */
#define D_CMN_FILE_LM_FMT_EXT    ".LM"		/* LM拡張子名 */
#define D_CMN_FILE_CFG_FMT_NAME  "UBOOT_CFG_F"	/* U-bootのconfigフォーマット名 */
#define D_CMN_FILE_CFG_FMT_EXT   ".CFG"			/* U-bootのconfigの拡張子名     */
#define D_CMN_FILE_CFG_PATH      "/cfg"			/* U-bootのconfigのディレクトリ */
#define D_CMN_FILE_UBOOTBIN      "U-bootF.BIN"	/* U-bootのヘッダファイル       */
#define D_CMN_FILE_UBOOTBIN_NAME "u-boot.bin"   /* U-bootファイル               */

	/* ファイル関連機能 End   */
	/* データベース変数チェック機能Start */
		/* デバック機能アドレス関連 */
#define D_CMN_DB_DEBUG_TFTPLMREADONOFF_ON 0x00000001	/* TFTPモード/LMモード有無読み込み:1 */
	/* データベース変数チェック機能End   */
/**define End  **/

/**enum Start**/
	/* 関数戻り値定義 */
typedef enum{
	/* ---NG番号:0--- */
	E_RET_OK=0,                     /* OK */
	E_RET_NG,                       /* 汎用 */
	E_RET_NG_NULL,                  /* 汎用:NULL */
	E_RET_NG_ZERO,                  /* 汎用:値0 */
	E_RET_NG_LOG_LVL,               /* 詳細:ログレベル値異常 */
	E_RET_NG_LOG_OUT_STRRESTRICTNUM,/* 詳細:ログ出力文字数制限超過 */
	E_RET_NG_LOG_LVL_OUTOFRANGE,    /* 詳細:ログレベル範囲外  */
	E_RET_NG_LOG_TMPBUFSIZEOVER,    /* 詳細:一時ログバッファサイズ超過 */
	E_RET_NG_FILE_OPENNG,           /* 詳細:ファイルOpenNG */
	E_RET_NG_FILE_NOTFOUND,         /* 詳細:ファイル見つからない */
	/* ---NG番号:10--- */
	E_RET_NG_FS_TYPE_SET,           /* 詳細:ファイルシステム種別設定NG */
	E_RET_NG_FS_FAT_SET,            /* 詳細:ファイルシステムFAT設定NG */
	E_RET_NG_FS_FAT_WRITE,          /* 詳細:ファイルシステムFAT書き込みNG */
	E_RET_NG_FS_FAT_READ,           /* 詳細:ファイルシステムFAT読み込みNG */
	E_RET_NG_DEV_PART_GET,          /* 詳細:デバイスパーテションGETNG */
	E_RET_NG_DEV_PART_SET,          /* 詳細:デバイスパーテションSETNG */
	E_RET_NG_DB_CRC32_NG,           /* 詳細:データベースのCRC32NG */
	E_RET_NG_DB_ACTREADSIZE_NG,     /* 詳細:データベースの読み込めたファイルサイズNG */
	E_RET_NG_CMD_SETENV_NG,         /* 詳細:コマンドsetenvNG */
	E_RET_NG_CMD_TFTPBOOT_NG,       /* 詳細:コマンドtftpbootNG */
	E_RET_NG_CMD_LMBOOT_NG,         /* 詳細:コマンドLMbootNG */
}E_RET_DEFINE;
/**enum End  **/

/**構造体 Start**/
	/* ファイル関連の構造体 */
typedef struct {
	INT_8   FileName[256];
	UINT_32 FileSize;
}S_FILELIST;
typedef struct {
	S_FILELIST FileList[64];
	UINT_32    FileNums;/* ファイルリストに格納したファイル数 */
}S_FILELISTS;
/* ファイルヘッダー */
typedef struct{
    UINT_32 crc32      ; /*<CRC32の計算結果                */
    UINT_32 offset     ; /*<読み出しoffsetアドレス         */
    UINT_32 offset_size; /*<読み出しoffsetアドレスのサイズ */
    UINT_16 fversion   ; /**<ファイル版数 Version          */
	UINT_8  rsv[2]     ; /*<空き領域2byte                  */
}S_CMN_FILE_HEADER;
/* システムLMデータのヘッダー */
typedef struct{
	UINT_32 crc32                   ;/**<CRC32の計算結果                  */
	UINT_16 maj_version             ;/**<ファイル版数 Major Version       */
	UINT_16 min_version             ;/**<ファイル版数 Minor Version       */
	UINT_16 bld_version             ;/**<ファイル版数 Build               */
	UINT_8  connect_file            ;/**<結合ファイルの有無 0:なし、1:あり*/
	UINT_8  connect_file_num        ;/**<結合ファイルの有無数             */
	UINT_32 file_size               ;/**<ファイルサイズ                   */
	INT_8   time_info[16]           ;/**<ファイル作成時刻情報[YYYY/MMDD-HHMMSS] */
	UINT_8  rsv[48]                 ;/**<空き領域:48byte                  */
	S_CMN_FILE_HEADER file_header[8];/**<ファイルヘッダ                   */
	UINT_8  rsv2[48]                ;/**<空き領域:48byte                  */
}S_CMN_HEADER_CONF; /* サイズ:0x100 */

/* U-bootのデータベース構造体ヘッダー 合計512Byte*/
typedef struct{
	UINT_32 crc32                   ;/**<CRC32の計算結果                  */
	UINT_16 maj_version             ;/**<ファイル版数 Major Version       */
	UINT_16 min_version             ;/**<ファイル版数 Minor Version       */
	UINT_16 rev_version             ;/**<ファイル版数 Revision Version    */
	UINT_8  rsv[246]                ;/**<空き領域:246byte                 */
	UINT_32 debug_func_addr         ;/**<デバック機能アドレス             */
	UINT_32 debug_func_addrSize     ;/**<デバック機能アドレスサイズ       */
	UINT_8  rsv2[248]               ;/**<空き領域:248byte                 */
}S_CMN_DATABASE_HEADER;
/* U-bootのデータベース構造体 デバック機能アドレス 合計256Byte*/
typedef struct{
	UINT_32 tftpLmReadOnOff       ;/**<TFTPモード/LMモード有無読み込み:1,LMREAD:0の機能有効無効 */
	UINT_32 ServerIpAddr          ;/**<本IPアドレスがTftpServerIP[PC]となる 例：setenv serverip 192.168.0.150 */
	UINT_32 tgtIpAddr             ;/**<本IPアドレスがターゲットIPアドレスとなる 例:setenv ipaddr 192.168.0.10 */
	UINT_32 kernelLoadAddr        ;/**<TFTPとLM共通 Linux Kernel Boot Load Addr */
	UINT_32 dtbLoadAddr           ;/**<TFTPとLM共通 Linux DTB Boot Load Addr */
	UINT_32 rootfsLoadAddr        ;/**<TFTPとLM共通 Linux RootFS Boot Load Addr */
	INT_8   tftpKernelFileName[64];/**<TFTP Linux Kernel File Name */
	INT_8   tftpDtbFileName[64]   ;/**<TFTP Linux DTB File Name */
	INT_8   tftprootfsFileName[64];/**<TFTP Linux RootFS FileName */
	UINT_16 logLevel              ;/**<ログレベルの定義 TRACE<->FATALまでのログ出力定義の変更可能 */
	UINT_8  rsv[38]               ;/**空き領域:38byte */
}S_CMN_DATABASE_DEBUG_FUNC_ADDR;
/* U-bootのデータベース構造体 全パラメータ 合計:768byte=ヘッダー:512Bbyte+デバック機能アドレス:256Byte*/
typedef struct{
	S_CMN_DATABASE_HEADER          DBheader;      /* U-bootのデータベース構造体ヘッダー              */
	S_CMN_DATABASE_DEBUG_FUNC_ADDR DebugFuncAddr; /* U-bootのデータベース構造体 デバック機能アドレス */
}S_CMN_DBALL;
/**構造体 End  **/

/**外部変数定義 Start**/
	/* データベースの構造体変数 */
extern S_CMN_DBALL CmnDbAll;  /* デバック機能 */
	/* ログ機能 Start*/
extern UINT_32 cmn_printf_loglvl_set_val;  /* ログレベル変数 */
	/* ログ機能 End*/
/**外部変数定義 End  **/

/**外部関数定義 Start**/
	/* 時間取得 */
UINT_64 rpi_lcmn_timer_get_boot_us(void);
	/* 時間取得 */
	/* ログ機能 Start*/
INT_32 rpi_lcmn_buf_printf( INT_8 , UINT_32 , const INT_8 * , UINT_32 , const INT_8 * , ...);
E_RET_DEFINE rpi_lcmn_lvl_chg( UINT_32 );
E_RET_DEFINE rpi_lcmn_log_FromRamToConsole(VOID);
	/* ログ機能 End*/
E_RET_DEFINE rpi_lcmn_conf_read(VOID);
VOID rpi_lcmn_init(VOID);  /* 初期化 */
	/* memory初期化 */
UINT_32 rpi_lcmn_memset( const VOID * , UINT_8 , UINT_32 );
UINT_32 rpi_lcmn_FmtFileNameGet( INT_8* ,INT_8* ,UINT_32 ,INT_8* ,UINT_32 ,const INT_8 * ,const INT_8 * ,INT_8 * );
UINT_32 rpi_lcmn_FileSizeGet( INT_8 * , const INT_8 * ,const INT_8 * , INT_8   *,UINT_32 * );
	/* fat Flash Read 機能 Start */
UINT_32 rpi_lcmn_fat_fsread   ( ULONG32 ,	loff_t ,	loff_t ,	const INT_8 *,	const INT_8 * ,	const INT_8 * , UINT_32* );
UINT_32 rpi_lcmn_fat_fsreadStr( ULONG32 ,	loff_t ,	loff_t ,	const INT_8 *,	const INT_8 * ,	const INT_8 * , UINT_32* ,	UINT_8 * );
UINT_32 rpi_lcmn_fat_buf_fsread( VOID *,loff_t *,loff_t ,loff_t ,const INT_8 *,const INT_8 * ,const INT_8 *);
	/* fat Flash Read 機能 End   */
UINT_32 rpi_lcmn_fat_fswrite( ULONG32 , UINT_32 , const INT_8 *, const INT_8 * , const INT_8 * );
E_RET_DEFINE rpi_lcmn_log_FromRamToFile( INT_8* , INT_8* , INT_8* );
E_RET_DEFINE rpi_lcmn_log_FromFileToRam( VOID );
VOID rpi_lcmn_log_Init( UINT_32 );

/**外部関数定義 End  **/

/**関数・変数のラッパー用define Start**/
	/* ログ機能 Start*/
		/* ログ表示可能範囲 */
		/* D_CMN_LOG_LVL_TRACE～D_CMN_LOG_LVL_FATALまで表示 */
#define D_CL_FATAL_DISP ( D_CMN_LOG_LVL_FATAL | D_CMN_LOG_LVL_ERROR | D_CMN_LOG_LVL_WARN  | D_CMN_LOG_LVL_INFO  | D_CMN_LOG_LVL_DEBUG | D_CMN_LOG_LVL_TRACE )
		/* D_CMN_LOG_LVL_TRACE～D_CMN_LOG_LVL_ERRORまで表示 */
#define D_CL_ERROR_DISP ( D_CMN_LOG_LVL_ERROR | D_CMN_LOG_LVL_WARN  | D_CMN_LOG_LVL_INFO  | D_CMN_LOG_LVL_DEBUG | D_CMN_LOG_LVL_TRACE )
		/* D_CMN_LOG_LVL_TRACE～D_CMN_LOG_LVL_WARNまで表示  */
#define D_CL_WARN_DISP  ( D_CMN_LOG_LVL_WARN  | D_CMN_LOG_LVL_INFO  | D_CMN_LOG_LVL_DEBUG | D_CMN_LOG_LVL_TRACE )
		/* D_CMN_LOG_LVL_TRACE～D_CMN_LOG_LVL_INFOまで表示  */
#define D_CL_INFO_DISP  ( D_CMN_LOG_LVL_INFO  | D_CMN_LOG_LVL_DEBUG | D_CMN_LOG_LVL_TRACE )
		/* D_CMN_LOG_LVL_TRACE～D_CMN_LOG_LVL_DEBUGまで表示 */
#define D_CL_DEBUG_DISP ( D_CMN_LOG_LVL_DEBUG | D_CMN_LOG_LVL_TRACE )
		/* D_CMN_LOG_LVL_TRACEのみ表示 */
#define D_CL_TRACE_DISP ( D_CMN_LOG_LVL_TRACE )

		/* トレースバッファプリントマクロ:ログレベルを識別し、バッファへかき出す */
		/* 下位のログレベルを指定した場合、上位のログも出力するものとする。 */
//#define D_TP( log_lvl, fmt ,... )  do{ if( log_lvl == D_CMN_LOG_LVL_TRACE ){ D_CL_TRACE( fmt,##__VA_ARGS__ ); }else if( log_lvl == D_CMN_LOG_LVL_DEBUG ){ D_CL_DEBUG( fmt,##__VA_ARGS__ );}else if( log_lvl == D_CMN_LOG_LVL_INFO ){D_CL_INFO( fmt,##__VA_ARGS__);}else if( log_lvl == D_CMN_LOG_LVL_WARN ){D_CL_WARN( fmt, ##__VA_ARGS__ );}else if( log_lvl == D_CMN_LOG_LVL_ERROR ){D_CL_ERROR( fmt,##__VA_ARGS__ );}else{D_CL_FATAL( fmt,##__VA_ARGS__ );}}while(0)
#if 0
#define D_TP( log_lvl, fmt ,... ) do{ rpi_lcmn_buf_printf(D_CMN_ON,log_lvl,__func__,__LINE__,fmt,##__VA_ARGS__);}while(0)
/* ダンプ情報の表示とかするときに時刻情報や行数などなしにする。 */
#define D_TPN( fmt, ... )  do{ rpi_lcmn_buf_printf(D_CMN_OFF,log_lvl,__func__,__LINE__,fmt,##__VA_ARGS__);}while(0)
#else
#define D_TP( log_lvl, fmt ,... ) \
do{ \
	printf("[f:%s,l:%d]"fmt,__func__,__LINE__,##__VA_ARGS__); \
	rpi_lcmn_buf_printf(D_CMN_ON,log_lvl,__func__,__LINE__,fmt,##__VA_ARGS__); \
}while(0)

#define D_TPN( fmt, ... ) \
do{ \
	printf("[f:%s,l:%d]"fmt,__func__,__LINE__,##__VA_ARGS__); \
	rpi_lcmn_buf_printf(D_CMN_OFF,log_lvl,__func__,__LINE__,fmt,##__VA_ARGS__); \
}while(0)
#endif
	/* ログ機能 End*/
/**関数・変数のラッパー用define End  **/

#endif/*_RPI_LOCAL_H_*/
