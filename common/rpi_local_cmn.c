// SPDX-License-Identifier: GPL-2.0+
/*
 * Logging support
 *
 * Copyright (c) 2019-2020 
 * Written by A.Kawashima 
 * 当改造により損害があった場合は無保証ですので自己負担となりますのでご注意お願いします。
 */

#include <rpi_local_cmn.h>

/* 関数外変数 Start */
S_CMN_DBALL CmnDbAll;  /* デバック機能 */
UINT_32 cmn_printf_loglvl_set_val;/* ログレベル変数 */
UINT_32 crc_table[256];
/* 関数外変数 End   */

/* 関数内マクロ Start*/
	/* ログ機能 Start */
#define DP_BUFLOG_MAX   1*1024*1024                 /* 10Mbyte->1Mbyte */
/* 10*1024*1024/23 =455879行=最大ログ保存可能行数 */
/*  最大ログの場合は以下を想定。、
    ヘッダ[時間、関数名、行数]:21文字 ユーザ定義:1文字 NULL1文字 = 23文字 */
/* 10*1024*1024/256=40960行=最小ログ保存可能行数 */
#define DP_BUF_MAX      256                          /* ログ一時バッファ */
	/* ログ機能 End   */
/* 関数内マクロ End  */
/* 関数内専用変数 Start */
	/* 変数の命名規則フォーマット:gp_xxx */
	/* g:global変数,p:private[static]変数,xxx:任意変数名 */
	/* ログ機能 Start */
static INT_8 gp_buf_log[DP_BUFLOG_MAX]; /* グローバルバッファログ */
static INT_8 gp_buf_log_tmp[DP_BUFLOG_MAX]; /* ローテ―トtempグローバルバッファログ */
	/* ログ機能 End   */
static UINT_32 gp_init_flg = D_CMN_OFF; /* グローバル初期化フラグ */
/* 関数内専用変数 End   */
/* 関数内専用関数 Start */
//static UINT_32 rpi_lcmn_memset( const VOID * , UINT_8 , UINT_32 );
static UINT_32 rpi_lcmn_FileListGet( INT_8 * , S_FILELISTS * , INT_32 *errcd );
//static UINT_32 rpi_lcmn_FileSizeGet( INT_8 * , INT_8 * , UINT_32 * );
static INT_32 rpi_lcmn_buf_p( const INT_8 *, ...);
static INT_32 rpi_lcmn_restrict_num_jdg( INT_8 * , UINT_32 *);
//static UINT_32 rpi_lcmn_fat_fsread( ULONG32 ,	UINT_32 ,	loff_t ,	loff_t ,	const INT_8 *,	const INT_8 * ,	const INT_8 *);
#ifdef    CONFIG_FAT_WRITE
//static UINT_32 rpi_lcmn_fat_fswrite( ULONG32 , UINT_32 , const INT_8 *, const INT_8 * , const INT_8 * );
#endif /* CONFIG_FAT_WRITE */
/* 関数内専用関数 End   */
/* 関数内マクロ Start*/
	/* ログ機能 Start */

#if 0
#define DP_TRC_PF( fmt, ... )  do{ rpi_lcmn_buf_p("[f:%s,l:%d]"fmt,__func__,__LINE__,##__VA_ARGS__);}while(0)
	/* ログ機能 End   */
	/*エラー用のprintf*/
#else
#define DP_TRC_PF( fmt, ... )  \
do{ \
	printf("[f:%s,l:%d]"fmt,__func__,__LINE__,##__VA_ARGS__); \
	rpi_lcmn_buf_p("[f:%s,l:%d]"fmt,__func__,__LINE__,##__VA_ARGS__); \
}while(0)
	/* ログ機能 End   */
#endif
	/*エラー用のprintf*/
#define DBGEP( fmt, ... )  do{ printf("[f:%s,l:%d]"fmt,__func__,__LINE__,##__VA_ARGS__);}while(0)

/* 関数内マクロ End  */

void make_crc_table(void) {
	UINT_32 i;
	UINT_32 c;
	INT_32  j;
    for ( i = 0; i < 256; i++) {
        c= i;
        for ( j= 0; j < 8; j++) {
            c = (c & 1) ? (0xEDB88320 ^ (c >> 1)) : (c >> 1);
        }
        crc_table[i] = c;
    }
}

UINT_32 mycrc32(UINT_8 *buf, SIZE_T len) {
    UINT_32 c = 0xFFFFFFFF;
	SIZE_T  i;
    for ( i = 0; i < len; i++)
	{
        c = crc_table[(c ^ buf[i]) & 0xFF] ^ (c >> 8);
    }
    return c ^ 0xFFFFFFFF;
}

UINT_32 rpi_lcmn_memset( const VOID * buf , UINT_8 val , UINT_32 bufsize )
{
	UINT_32 ret = E_RET_OK;
	INT_8  *_buf= (INT_8 *)buf;
	UINT_32 count = 0;
	
	if( _buf == NULL )
	{
		DBGEP("_buf NULL\n");
		return E_RET_NG_NULL;
	}
	if( bufsize == 0 )
	{
		DBGEP("bufsize Zero\n");
		return E_RET_NG_ZERO;
	}
	
	for( count = 0 ; count < bufsize ; count++,_buf++ )
	{
		*_buf = val;
	}
	
	return ret;
}

//ディレクトリ指定でのファイルリストの取得
//FATのみ対応[fs_ls_genericはFATのみのため]
static UINT_32 rpi_lcmn_FileListGet( INT_8 *DirName , S_FILELISTS * FileLists , INT_32 *errcd )
{
	UINT_32 ret = E_RET_OK;
	struct fs_dir_stream *dirs = NULL;
	struct fs_dirent     *dent = NULL;
	INT_32              nfiles = 0;
	INT_32              ndirs  = 0;
	INT_32              nfiles_max = 0;
	
	/* 実行条件チェック */
		/* ディレクトリ名NULLの場合 */
	if( DirName  == NULL )
	{
		DBGEP("DirName NULL\n");
		return E_RET_NG_NULL;
	}
	if( FileLists == NULL )
	{
		DBGEP("FileLists NULL\n");
		return E_RET_NG_NULL;
	}
	if( errcd    == NULL )
	{
		DBGEP("errcd NULL\n");
		return E_RET_NG_NULL;
	}
	/* 初期化 */
	*errcd = 0;
	
	dirs = fs_opendir(DirName);
	if (!dirs)
	{
		*errcd = -errno;
		DBGEP("File Open Error errno=%d\n", -errno);
		return E_RET_NG_FILE_OPENNG;
	}
	
	nfiles_max = sizeof(FileLists->FileList)/sizeof(FileLists->FileList[0]);
	
	while ((dent = fs_readdir(dirs)))
	{
		if (dent->type == FS_DT_DIR)
		{
			//printf("            %s/\n", dent->name);
			ndirs++;
		}
		else
		{
			//printf(" %8lld   %s\n", dent->size, dent->name);
			if( nfiles < nfiles_max )
			{
				snprintf( FileLists->FileList[nfiles].FileName , sizeof(FileLists->FileList[nfiles].FileName) ,"%s" , dent->name );
				FileLists->FileList[nfiles].FileSize = dent->size;
				FileLists->FileNums++;
			}
			else
			{
				DBGEP("nfiles is FileLists->FileList define Over.\n");
				fs_closedir(dirs);
				return E_RET_NG_FILE_NOTFOUND;
			}
			nfiles++;
		}
	}

	fs_closedir(dirs);

	//printf("\n%d file(s), %d dir(s)\n\n", nfiles, ndirs);

	return ret;
}
/* フォーマット指定のファイル名の取得処理
  @[In]DirName            :ディレクトリ名
  @[In]filename_format    :ファイルフォーマット
  @[In]filename_formatLen :ファイルフォーマットの長さ
  @[In]extension          :ファイルフォーマット
  @[In]extensionLen       :ファイルフォーマットの長さ
  @[IN]dev_name           :U-bootのデバイス名
  @[IN]dev_part_str       :U-bootのパーツ文字列
  @[Out]FileName          :ファイル名
*/
UINT_32 rpi_lcmn_FmtFileNameGet(	INT_8*        DirName ,
									INT_8*        filename_format ,
									UINT_32       filename_formatLen , 
									INT_8*        extension ,
									UINT_32       extensionLen ,
									const INT_8 * dev_name , 
									const INT_8 * dev_part_str , 
									INT_8 *       FileName )
{
	UINT_32 ret                         = E_RET_NG_FILE_NOTFOUND;
	S_FILELISTS  FileLists;
	INT_32       errcd                  = 0;
	UINT_32      filecnt                = 0;
	INT_32       str_ret                = 0;
	INT_8        *s_str_ret             = NULL;
	INT_8*       tmp_extension          = NULL;
	INT_8*       tmp_filename_format    = NULL;
	UINT_32      tmp_filename_formatLen = 0;
	UINT_32      tmp_extensionLen       = 0;
	/* 0初期化 */
	rpi_lcmn_memset( &FileLists , 0x00 , sizeof(FileLists) );

	if( ( FileName == NULL ) || ( filename_format == NULL ) || ( extension == NULL ) )
	{
		return E_RET_NG_NULL;
	}
	tmp_filename_formatLen = (UINT_32)strlen( filename_format );
	if( tmp_filename_formatLen == 0 )
	{
		return E_RET_NG_ZERO;
	}
	tmp_extensionLen = (UINT_32)strlen( extension );
	if( tmp_extensionLen == 0 )
	{
		return E_RET_NG_ZERO;
	}
	if (fs_set_blk_dev(dev_name, dev_part_str, FS_TYPE_FAT))
	{
		//DP_TRC_PF("fs_set_blk_dev 1\n");
		return E_RET_NG_DEV_PART_SET;
	}
	/* ファイルリストの取得 */
	/* ファイルリストのファイル名と引数のファイル名から一致すれば、ファイル取得OK */
	/* 一致以外はNGとする */
	ret = rpi_lcmn_FileListGet( DirName , &FileLists , &errcd );
	if( ret == E_RET_OK )
	{
		ret = E_RET_NG_FILE_NOTFOUND;
		for( filecnt = 0 ; filecnt < FileLists.FileNums ;filecnt++ )
		{
			/* ファイル名の拡張子を取得 */
			tmp_extension = strstr( FileLists.FileList[filecnt].FileName ,"." );
			if( tmp_extension == NULL )
			{
				/* 拡張子がない場合次へ */
				//DP_TRC_PF("tmp_extension NULL\n");
				continue;
			}
			//DP_TRC_PF("tmp_extension=%s\n",tmp_extension);
			/* ファイル名の拡張子が指定された拡張子かチェック */
			str_ret = strncmp( tmp_extension , extension , tmp_extensionLen );
			if( str_ret != 0 )
			{
				/* 不一致の場合は次へ */
				//DP_TRC_PF("str_ret==0\n");
				continue;
			}
			//DP_TRC_PF("str_ret==%d\n", str_ret );
			/* ファイル名フォーマットを取得チェック */
			tmp_filename_format = strstr( FileLists.FileList[filecnt].FileName ,filename_format );
			if( tmp_filename_format == NULL )
			{
				/* ファイルフォーマットがない場合次へ */
				//DP_TRC_PF("tmp_filename_format== NULL\n" );
				continue;
			}
			//DP_TRC_PF("tmp_filename_format=%s\n", tmp_filename_format );
			/* ファイル名フォーマット一致しているかチェック */
			str_ret = strncmp( tmp_filename_format , filename_format , tmp_filename_formatLen );
			if( str_ret == 0 )
			{
				//DP_TRC_PF("str_ret= 0\n");
				/* 指定のファイル名発見した場合 */
				
				/* ファイル名取得 */
				s_str_ret = strcpy( FileName , FileLists.FileList[filecnt].FileName );
				if( s_str_ret == NULL )
				{
					DBGEP("strcpy NG.\n");
					return ret;
				}
				ret = E_RET_OK;
				
				break;
			}
			//DP_TRC_PF("str_ret==%d\n", str_ret );
		}
		if( filecnt >= FileLists.FileNums )
		{
			DBGEP("filecnt is FileLists.FileNums define Over.\n");
		}
	}
	DBGEP("FileName=%s\n",FileName);
	return ret;
}
//ファイルサイズの取得処理
/*
	@[IN]DirName						:ディレクトリ名
	@[IN]dev_name                       :U-bootのデバイス名
	@[IN]dev_part_str                   :U-bootのパーツ文字列
	@[IN]FileName                       :ファイル名[ディレクトリなし]
	@[OUT]fsize							:ファイルサイズ
*/
UINT_32 rpi_lcmn_FileSizeGet(	INT_8 *DirName , 
								const INT_8 * dev_name , 
								const INT_8 * dev_part_str , 
								INT_8 *FileName ,
								UINT_32 *fsize )
{
	UINT_32 ret        = E_RET_NG_FILE_NOTFOUND;
	S_FILELISTS  FileLists;
	INT_32       errcd = 0;
	UINT_32      filecnt = 0;
	INT_32       str_ret = 0;
	
	/* 0初期化 */
	rpi_lcmn_memset( &FileLists , 0x00 , sizeof(FileLists) );

	if( fsize == NULL )
	{
		return E_RET_NG_NULL;
	}
	if (fs_set_blk_dev(dev_name, dev_part_str, FS_TYPE_FAT))
	{
		//DP_TRC_PF("fs_set_blk_dev 1\n");
		return E_RET_NG_DEV_PART_SET;
	}
	/* ファイルリストの取得 */
	/* ファイルリストのファイル名と引数のファイル名から一致すれば、ファイル取得OK */
	/* 一致以外はNGとする */
	ret = rpi_lcmn_FileListGet( DirName , &FileLists , &errcd );
	if( ret == E_RET_OK )
	{
		ret = E_RET_NG_FILE_NOTFOUND;
		for( filecnt = 0 ; filecnt < FileLists.FileNums ;filecnt++ )
		{
			/* ファイル名一致しているかチェック */
			str_ret = strcmp( FileLists.FileList[filecnt].FileName , FileName );
			if( str_ret == 0 )
			{
				/* 指定のファイル名発見した場合 */
				
				/* ファイルサイズ取得 */
				*fsize = FileLists.FileList[filecnt].FileSize;
				ret = E_RET_OK;
				
				break;
			}
		}
		if( filecnt >= FileLists.FileNums )
		{
			DBGEP("filecnt is FileLists.FileNums define Over.\n");
		}
	}
	return ret;
}
/* ファイル読み込み処理[ユーザがメモリ領域指定] */
/*
	@[IN]addr							:システムメモリ内のアドレス
	@[OUT]len                            :システムメモリ内のアドレスの長さ
	@[IN]Offset :file_addless postion   :ファイルの相対アドレス位置
	@[IN]OffsetLen                      :ファイルの相対アドレス位置の長さ
	@[IN]dev_name                       :U-bootのデバイス名
	@[IN]dev_part_str                   :U-bootのパーツ文字列
	@[IN]filename                       :ファイル名[ディレクトリ付き]
*/
//static UINT_32 rpi_lcmn_fat_fsread( 
UINT_32 rpi_lcmn_fat_fsread( 
	ULONG32 addr ,
	loff_t  Offset,
	loff_t  OffsetLen,
	const INT_8 * dev_name , 
	const INT_8 * dev_part_str , 
	const INT_8 *filename,
	UINT_32* read_len)
{
	UINT_32 ret = E_RET_OK;
	INT_32  fs_ret;
	loff_t  len_read;
	ULONG32 time;

	if (fs_set_blk_dev(dev_name, dev_part_str, FS_TYPE_FAT))
	{
		DP_TRC_PF("fs_set_blk_dev 1\n");
		return 1;
	}

	time = get_timer(0);
	fs_ret = fs_read(filename, addr, Offset, OffsetLen, &len_read);
	time = get_timer(time);
	if (fs_ret < 0)
	{
		DP_TRC_PF("fs_read %d\n",fs_ret);
		return 1;
	}
	else
	{
		DP_TRC_PF("fs_read OK %d\n",fs_ret);
		ret = E_RET_OK;
	}

	printf("%llu bytes read in %lu ms", len_read, time);
	if (time > 0) {
		puts(" (");
		print_size(div_u64(len_read, time) * 1000, "/s");
		puts(")");
	}
	*read_len = (UINT_32) len_read;
	puts("\n");
	
	return ret;
}
/* ファイル情報を構造体へ読み込み処理[ユーザがメモリ領域指定] */
/*
	@[IN]addr							:システムメモリ内のアドレス
	@[OUTlen                            :システムメモリ内のアドレスの長さ
	@[IN]Offset :file_addless postion   :ファイルの相対アドレス位置
	@[IN]OffsetLen                      :ファイルの相対アドレス位置の長さ
	@[IN]dev_name                       :U-bootのデバイス名
	@[IN]dev_part_str                   :U-bootのパーツ文字列
	@[IN]filename                       :ファイル名[ディレクトリ付き]
	@[OUT]str                            :読み出し先の構造体
*/
//static UINT_32 rpi_lcmn_fat_fsread( 
UINT_32 rpi_lcmn_fat_fsreadStr( 
	ULONG32 addr ,
	loff_t  Offset,
	loff_t  OffsetLen,
	const INT_8 * dev_name , 
	const INT_8 * dev_part_str , 
	const INT_8 *filename ,
	UINT_32* read_len ,
	UINT_8* str )
{
	UINT_32 ret = E_RET_OK;

	ret = rpi_lcmn_fat_fsread( addr , Offset, OffsetLen , dev_name , dev_part_str , filename , read_len);
	
	if( ret == E_RET_OK )
	{
		if( str == NULL )
		{
			DP_TRC_PF("str NULL\n");
			return E_RET_NG_NULL;
		}
		/*  引数:strへの書き込みをするのはメモリ破壊の可能性もあるため,strとlenはユーザ保証とする。 */
		memcpy( str, (UINT_8 *)addr , *read_len );
	}
	else
	{
		DP_TRC_PF("rpi_lcmn_fat_fsread %d\n",ret);
	}
	
	return ret;
}
#ifdef CONFIG_FAT_WRITE
/* ファイル書き込み処理 */
/* U-boot上のメモリに一時展開し、ファイル書き込みを行う。使用するメモリ領域はユーザ指定 */
/*
	@addr         :システムメモリ内のアドレス
	@len          :システムメモリ内のアドレスの長さ
	@dev_name     :U-bootのデバイス名
	@dev_part_str :U-bootのパーツ文字列
	@filename     :ファイル名[ディレクトリ付き]
*/
UINT_32 rpi_lcmn_fat_fswrite( 
	ULONG32 addr ,
	UINT_32 len , 
	const INT_8 * dev_name , 
	const INT_8 * dev_part_str , 
	const INT_8 *filename )
{
	loff_t size = 0;
	UINT_32 ret = 0;
	struct blk_desc *dev_desc = NULL;
	disk_partition_t info;
	int dev = 0;
	int part = 1;
	void *buf = NULL;

	/* 初期化 */
	rpi_lcmn_memset( &info , 0x00 , sizeof(info) );
	
	part = blk_get_device_part_str(dev_name, dev_part_str, &dev_desc, &info, 1);
	if (part < 0)
	{
		DBGEP("blk_get_device_part_str Error.part=%d dev_name=%s dev_part_str=%s\n",part,dev_name,dev_part_str);
		return E_RET_NG_DEV_PART_GET;
	}

	dev = dev_desc->devnum;

	if (fat_set_blk_dev(dev_desc, &info) != 0)
	{
		DBGEP("\n** Unable to use %s %d:%d for fatwrite **\n",
			dev_name, dev, part);
		return E_RET_NG_FS_FAT_SET;
	}

	buf = map_sysmem(addr, len);
	ret = file_fat_write(filename, buf, 0, len, &size);
	unmap_sysmem(buf);
	if (ret < 0)
	{
		DBGEP("\n** Unable to write \"%s\" from %s %d:%d **\n",
			filename, dev_name, dev, part);
		return E_RET_NG_FS_FAT_WRITE;
	}

	printf("%llu bytes written\n", size);

	return E_RET_OK;
}

#endif

/*
  LMヘッダを読み出し、構造体へコピーする
 */
E_RET_DEFINE rpi_lcmn_lm_header_read(
	const INT_8 * dev_name , 
	const INT_8 * dev_part_str , 
	const INT_8 * filename ,
	S_CMN_HEADER_CONF * lmheader
)
{
	E_RET_DEFINE ret = E_RET_OK;
	
	
	
	return ret;
}
/* bufへ格納する。 */
/* TODO:ログローテートできるようにする */
static INT_32 rpi_lcmn_buf_p( const INT_8 *format, ...)
{
	/* NOTE: attention to buffer overflow */
	INT_8 *p = NULL;
	INT_8 *pp= NULL;
	INT_32 ret = 0;
	INT_8 buf[DP_BUF_MAX] = {0};
	INT_32 len_str_buf   = 0;
	INT_32 len_buf       = 0;
	INT_32 len_write_jdg = 0;
	va_list ap;
#if 0 //ログローテートはD_CMN_LOG_INIT_STRのフォーマットのindexのインクリメントする機構を共通化していないので廃止。
	INT_32 req_str_len   = 0;
	INT_8 *tmp_p         = NULL;
	INT_8 *tmp_pp        = NULL;
	INT_8 *tmp_p2        = NULL;
	INT_8 *tmp_pp2       = NULL;
	INT_32 newline_len   = 0;
	INT_32 tmp_p2_len    = 0;
#endif
	va_start(ap, format);
	ret = vsnprintf(buf, sizeof(buf) ,format, ap);
	va_end(ap);

	if( ret >= (DP_BUF_MAX-1) )
	{
		/* エラー表示 */
		DBGEP("The input strings is more than %d. strings overflow.\n",DP_BUF_MAX-1);
		return ret;
	}
	/* バッファ用のグローバル変数のポインタを代入 */
	pp = gp_buf_log;
	
	p = buf;
	/* gp_buf_logの文字列の長さ */
	len_str_buf= strlen(pp);
	/* 引数:bufの文字列の長さ */
	len_buf    = strlen(p);
	/* ( gp_buf_logの文字列の長さ + 引数:bufの文字列の長さ ) <= DP_BUFLOG_MAX であればコピーしないようにする。 */
	len_write_jdg = len_str_buf + len_buf;
	
	if( len_write_jdg >= DP_BUFLOG_MAX )
	{
		DBGEP("DP_BUFLOG_MAX_MAXSIZE Over %d \r\n",len_write_jdg);
		return -1;

#if 0 //ログローテートはD_CMN_LOG_INIT_STRのフォーマットのindexのインクリメントする機構を共通化していないので廃止。
		/* TODO:ログローテーション更新方法 */
			/* 実装仕様
			1.必要な文字列の全体サイズ - ログRAMサイズ= 必要な文字数
			2.必要な文字数:b
		    3.ログRAMの"\n"までの文字数をa+=aとして算出[aの初期化値0]
		    4.aがbより大きいか？チェック
		    5.チェックNGの場合,3へ戻る
		    6.チェックOKの場合、ログRAMの先頭ポインタ+a ～b -> cへコピー
		    7.ログRAMをALL0初期化
		    8.ログRAM -> ログRAMの先頭ポインタへコピー
			*/
		req_str_len = len_write_jdg - DP_BUFLOG_MAX;
		tmp_pp = gp_buf_log;
		/*  */
		tmp_p = strstr( tmp_pp ,D_CMN_LOG_NEWLINE_CHK );

		while(( tmp_p = strstr( tmp_pp ,D_CMN_LOG_NEWLINE_CHK ) ) != NULL)
		{
			newline_len += ( ( tmp_p - tmp_pp ) + 1 );/* ログRAMの現在の\nの先頭アドレス - ログRAMの現在の先頭アドレス + \nの文字列数 */
			if( req_str_len > newline_len )
			{
				tmp_pp++; /* 発見した改行コードは時間の検索から削除するため,インクリメントする */
				continue;
			}
			
			memset(&gp_buf_log_tmp , 0x00 ,sizeof(gp_buf_log_tmp));
			tmp_pp2 = gp_buf_log_tmp;
			tmp_p2  =  (INT_8 *)( (INT_8 *)&gp_buf_log + newline_len );
			tmp_p2_len = strlen(tmp_p2) + req_str_len;
			(VOID)strncat( tmp_pp2 , tmp_p2 ,tmp_p2_len );
			
			puts(gp_buf_log);//ローテート前のダンプ
			
			memset(&gp_buf_log , 0x00 ,sizeof(gp_buf_log));
			tmp_pp = gp_buf_log;
			(VOID)strncat( tmp_pp , tmp_pp2 ,tmp_p2_len );
			
			puts(gp_buf_log);//ローテート後のダンプ
			DBGEP("Log Rotate OK\n");
			break;
		}
#endif
	}
	/* DP_BUFLOG_MAXの'\0'へ引数:bufの文字列をコピー */
	(VOID)strncat( pp , p ,len_write_jdg);

	//printf("gp_buf_log:%s.\r\n",gp_buf_log);
	return ret;
}
static INT_32 rpi_lcmn_restrict_num_jdg( INT_8 * buf , UINT_32 *workbuf_len)
{
	if( buf == NULL)
	{
		/* NULLチェックNG */
		return E_RET_NG_NULL;
	}
	*workbuf_len = strlen(buf);
	if( *workbuf_len>=DP_BUF_MAX )
	{
		/* ログ出力文字数制限超過 */
		return E_RET_NG_LOG_OUT_STRRESTRICTNUM;
	}
	return E_RET_OK;
}
UINT_64 rpi_lcmn_timer_get_boot_us(void)
{
	UINT_64 count = 0;
	UINT_32 chi = 0;
	UINT_32 clo = 0;
	
	chi = *RPI_CONFIG_SYS_TIMER_COUNTERHI;
	clo = *CONFIG_SYS_TIMER_COUNTER;
	/* 桁上がりチェック */
	if( chi!= *RPI_CONFIG_SYS_TIMER_COUNTERHI )
	{
		chi = *RPI_CONFIG_SYS_TIMER_COUNTERHI;
		clo = *CONFIG_SYS_TIMER_COUNTER;
		
	}
	
	count = chi;
	count = count<<32;
	count += clo;
	//printf("chi:0x%x/0x%x cho:0x%x/0x%x\n",RPI_CONFIG_SYS_TIMER_COUNTERHI,*RPI_CONFIG_SYS_TIMER_COUNTERHI,CONFIG_SYS_TIMER_COUNTER,*CONFIG_SYS_TIMER_COUNTER);
	return count;
}
/* ログレベル、関数名、行数をログに追加、ログをバッファに格納する関数[TODO:現在はUART出力をputsで実現,将来的にRAMへコピーできるようにする] */
/*
   ログの全体量:NULL文字込みで256文字
   [00:00:00.123456,FATAL,1234567890123456789012345678901234567890123456789012345678901234:012345]ユーザ定義の文字列
    |             | |   | |                                                              | |    |
    |             | |   | |                                                              |└---┘行数:5桁
    |             | |   | |                                                              |
    |             | |   |└-------------------------------------------------------------┘関数名:64文字
    |             | |   |
    |             |└--┘ログ名:5文字
    |             |
   └------------┘時間15文字[時:分:秒.マイクロ秒] MIN～MAX:00:00:00.000000～99:59:59.999999まで有効。MAX超えたらMINからはじまる。[U-bootだけで4日以上そのままって通常ありえないので問題ない...]
   
    計:95文字
    残り:161
    終端:'\0'なのでログ名、関数名、行数が最大値を示すときユーザが使用できる領域は160文字
    また、ログ名、関数名、行数が最大値を下回る場合はユーザが使用できる領域が増える。
    最低値は以下のようになる。、
    ログ名:4,関数名1,行数:1桁の場合
    256-21=235文字となるのでユーザはログ名、関数名、行数に左右されるため以下の範囲まで文字列を表現できる。
    160～235文字まで[NULL文字含む]
    TODO:ログ出力番号の追加[ログが前後した場合の考慮...U-bootだとスレッドないからいらないか...]
 */
INT_32 rpi_lcmn_buf_printf( INT_8 output_time_flg , UINT_32 log_lvl ,const INT_8 * func_name , UINT_32 file_line , const INT_8 * fmt, ...)
{
	va_list			pvar;
	va_list			tmppvar;
	INT_8			workbuf[DP_BUF_MAX] ={0};
	UINT_32			workbuf_len = 0;
	UINT_32			jdg_workbuf_len = 0;
	INT_32			ret        = 0;
	UINT_64			total      = 0;/* マイクロ秒 */
	ULONG32			htime      = 0;/* 時 */
	ULONG32			outhtime   = 0;/* 時 */
	ULONG32			mintime    = 0;/* 分 */
	ULONG32			outmintime = 0;/* 分 */
	ULONG32			stime      = 0;/* 秒 */
	ULONG32			outstime   = 0;/* 秒 */
	UINT_64			ustime     = 0;/* マイクロ秒 */
	ULONG32			outustime  = 0;/* マイクロ秒 */
	INT_8*			str_log_name[]={ "TRACE","DEBUG","INFO","WARN","ERROR","FATAL"};
	UINT_32			log_lvl_disp[]={
									D_CL_TRACE_DISP,
									D_CL_DEBUG_DISP,
									D_CL_INFO_DISP ,
									D_CL_WARN_DISP ,
									D_CL_ERROR_DISP,
									D_CL_FATAL_DISP };
	UINT_32			log_lvl_num[] ={
									D_CMN_LOG_LVL_TRACE,
									D_CMN_LOG_LVL_DEBUG,
									D_CMN_LOG_LVL_INFO ,
									D_CMN_LOG_LVL_WARN ,
									D_CMN_LOG_LVL_ERROR,
									D_CMN_LOG_LVL_FATAL };
	INT_32			log_lvl_cnt = 0;
	INT_32			log_lvl_NG_cnt = 0;
	//DP_TRC_PF("<IN>[DEBUG] timeFlg:0x%x,loglvl=0x%x funcname=%s,file_line=%d\n",output_time_flg,log_lvl,func_name,file_line);
	/* 時刻出力フラグがONの場合か？ */
	if( output_time_flg != D_CMN_OFF )
	{
		/* 時間情報の整形	*/
		/* u-bootでは起動時からの起動時間[usオーダー]を抽出する */
		/* TODO:時間抽出の処理がおかしいのでFix要 */
		/* rpi_lcmn_timer_get_boot_us関数はusオーダー[レジスタのタイマー周期が1Mhz固定なので秒=1/Hzなので秒計算には10^6必要 */
		total   = ustime  = rpi_lcmn_timer_get_boot_us();
		/* uicro second->秒変換 */
		total  /= D_CMN_LOG_SECOND_TIME;
		/* 秒を1分間単位で算出 */
		stime   = total%D_CMN_LOG_1MIN_TIME;
		/* 秒を分へ変換 */
		total  /= D_CMN_LOG_1MIN_TIME;
		/* 分を1時間単位で算出 */
		mintime = total%D_CMN_LOG_1HOUR_TIME;
		/* 分を時間へ変換 */
		total  /= D_CMN_LOG_1HOUR_TIME;
		/* 時間を算出 */
		htime   = total%D_CMN_LOG_1HOUR_TIME;
		/* 時間-日へ変換[不要] */
		//total  /= D_CMN_LOG_1DAY_TIME;
		
		outustime  = ustime%D_CMN_LOG_MICRO_TIME;
		outstime   = stime;
		outmintime = mintime;
		outhtime   = htime;
		
		/* フォーマット:時:分:秒.マイクロ秒 ※bootloaderなので年月日は不要 */
		snprintf(workbuf, sizeof(workbuf), "[%02ld:%02ld:%02ld.%06ld",outhtime,outmintime,outstime,outustime );
		ret = rpi_lcmn_restrict_num_jdg(workbuf , &workbuf_len );
		if( ret != E_RET_OK )
		{
			DP_TRC_PF("[ERROR] ret=0x%x workbuf_len=%d\n",ret,workbuf_len);
			return ret;
		}
	
		/* ログレベルごとにループ */
		for( log_lvl_cnt = 0; log_lvl_cnt < sizeof(log_lvl_num)/sizeof(log_lvl_num[0]) ; log_lvl_cnt++ )
		{
			/* 引数:ログレベルと等しいか？ */
			if( log_lvl_num[log_lvl_cnt] == log_lvl )
			{
				/* グローバル変数で設定したログレベルの範囲に該当するか？ */
				if((cmn_printf_loglvl_set_val & log_lvl_disp[log_lvl_cnt] )!= 0 )
				{
					/* 該当するログレベルの文字列を生成する */
					/* ログレベル、関数名、行数の文字列は最大値に満たない場合は後ろが切り詰められるようにする */
					snprintf(workbuf + workbuf_len,sizeof(workbuf)-workbuf_len,",%.5s,%.64s:%.5d]",str_log_name[log_lvl_cnt],func_name, file_line);
					ret = rpi_lcmn_restrict_num_jdg(workbuf , &workbuf_len );
					if( ret != E_RET_OK )
					{
						DP_TRC_PF("[ERROR] ret=0x%x workbuf_len=%d\n",ret,workbuf_len);
						return ret;
					}
					/* 文字列作成後ループを抜ける。 */
					break;
				}
				else
				{
					/* ログレベルの範囲に該当しないものは非表示にする */
					//DP_TRC_PF("[ERROR] Log Level Out Of Range loglvl_set_val/log_lvl_disp[%d]=0x%x/0x%x \n"
					//	,log_lvl_cnt,cmn_printf_loglvl_set_val,log_lvl_disp[log_lvl_cnt]);
					return E_RET_NG_LOG_LVL_OUTOFRANGE;
				}
			}
			else
			{
				log_lvl_NG_cnt++;
			}
		}
		
		/* 1つも該当するログレベルがない場合 */
		if( log_lvl_NG_cnt == sizeof(log_lvl_num)/sizeof(log_lvl_num[0]) )
		{
			/* ログレベル値異常 */
			DP_TRC_PF("[ERROR] log_lvl_cnt=0x%x USER:%s\n",log_lvl_cnt,workbuf);
			return E_RET_NG_LOG_LVL;
		}
	}
	/* ログ出力用のバッファのサイズが0未満の場合 */
	/* 補足:基本的に表示文字列長を固定にしたので条件を満たすことは基本的にないはず... */
	if( (sizeof(workbuf)-workbuf_len) <= 0 )
	{
		/* ログ出力用のバッファのサイズ超過するのでログ出力させないようにする。 */
		DP_TRC_PF("[ERROR] tmp buf OverFlow!! Over length=%d USER:%s\n",sizeof(workbuf)-workbuf_len,workbuf);
		return E_RET_NG_LOG_TMPBUFSIZEOVER;
	}
	/* ユーザ指定メッセージの整形	*/
	/* ユーザ指定メッセージの整形後に必要な文字列長を算出し、一時バッファ制限の数[160文字]を超過していないか？チェックする */
	/* チェックNGの場合は先頭から160文字までをコピーし出力するものとする */
	/* なお、vsnprintf関数はバッファのサイズまで格納してくれる安全な関数に見えるが、バッファサイズ超過のパターンが実装上あり得るため */
	/* バッファオーバーランするのでログ機能の使用者側で気を付ける必要がある。 */
	/* 参考資料 */
	/* vsnprintf | Programming Place Plus　Ｃ言語編　標準ライブラリのリファレンス */
	/* https://programming-place.net/ppp/contents/c/appendix/reference/vsnprintf.html*/
	/* [C] 意外と知られていないva_copy  */
	/* https://jumble-note.blogspot.com/2012/09/c-vacopy.html */
	va_start(pvar, fmt);
	/* 一度pvarを可変長引数を使用した関数で使用すると再利用できないのでpvar->tmppvarへコピーします。 */
	va_copy(tmppvar, pvar);
	/* コピー後のtmppvarを使用してユーザ指定メッセージがどれだけ文字を必要としているかチェックを行う。 */
	jdg_workbuf_len = vsnprintf(NULL, 0, fmt, tmppvar);
	
	/* 本当はmalloc使用すればユーザが必要としている分のログ取得用の一時バッファを可変長にできたが、*/
	/* u-bootの追加optionのかつメモリ確保と解放が多いと速度も遅くなるので採用しなかった */
	
	//デバック用だがva_staet範囲なのでバッファの長さを超過した条件の場合はDP_TRC_PFで使用しているfmtとapが反応するので表示がおかしくなる。
	//DP_TRC_PF("[DBG] tmp buf OverLength UserRequest/BufRemaining=%d/%d\n",jdg_workbuf_len+1,sizeof(workbuf)-workbuf_len);
	/* ユーザ指定メッセージが必要とする長さ+NULL文字分 >= ログ表示用バッファの残りの長さ[NULL文字含む] */
	/* [sizeof(配列)なのでNULL文字含む-ユーザ定義以外の文字列[NULL含まない]] */
	if( (jdg_workbuf_len+1) >= (sizeof(workbuf)-workbuf_len) )
	{
		/* ログ非表示かつva_endで終了させてからユーザ指定されたログ出力用のバッファが指定より多いサイズを超過するがあることを明示。 */
		va_end(pvar);
		va_end(tmppvar);
		/* ログ出力用のバッファのサイズ超過するのでログ出力させないようにする。ユーザ要求のサイズと残りバッファのサイズをダンプ。 */
		DP_TRC_PF("[ERROR] tmp buf LengthOver Prevention UserRequest/BufRemaining=%d/%d USER%s\n",jdg_workbuf_len+1,sizeof(workbuf)-workbuf_len,workbuf);
		return E_RET_NG_LOG_TMPBUFSIZEOVER;
	}
	vsnprintf(workbuf + workbuf_len, sizeof(workbuf)-workbuf_len, fmt, pvar);
	va_end(pvar);
	va_end(tmppvar);
	ret = rpi_lcmn_restrict_num_jdg(workbuf , &workbuf_len );
	if( ret != E_RET_OK )
	{
		DP_TRC_PF("[ERROR] ret=0x%x USER:%s\n",ret,workbuf);
		return ret;
	}
    /* ユーザログデータの出力 */
	rpi_lcmn_buf_p(workbuf);
	
	//DP_TRC_PF("<OUT>[DEBUG] OK\n",ret);
	
	return E_RET_OK;
}

/* ログレベルの変更  */
E_RET_DEFINE rpi_lcmn_lvl_chg( UINT_32 log_lvl )
{
	switch( log_lvl )
	{
	case D_CMN_LOG_LVL_FATAL:
	case D_CMN_LOG_LVL_ERROR:
	case D_CMN_LOG_LVL_WARN :
	case D_CMN_LOG_LVL_INFO :
	case D_CMN_LOG_LVL_DEBUG:
	case D_CMN_LOG_LVL_TRACE:
		break;
	default:
		return E_RET_NG;
	}
	cmn_printf_loglvl_set_val = log_lvl;
	return E_RET_OK;
}

/* RAMにあるログをコンソールに吐き出す。  */
E_RET_DEFINE rpi_lcmn_log_FromRamToConsole(VOID)
{
	UINT_32 ret = E_RET_OK;
	const INT_8 *p;
	p = (INT_8 *)gp_buf_log;
	puts(p);
	return ret;
}
/* RAMにあるログをファイルに吐き出す。  */
/*
 */
E_RET_DEFINE rpi_lcmn_log_FromRamToFile( INT_8* dev_name, INT_8* dev_part_str , INT_8* FileName )
{
	UINT_32 ret = E_RET_OK;
	const INT_8 *p;
	
	p = (INT_8 *)gp_buf_log;
	ret = rpi_lcmn_fat_fswrite( 
		(ULONG32)p ,
		strlen( p ) , 
		dev_name , 
		dev_part_str , 
		FileName );
	
	if( ret != 0 )
	{
		D_TP( D_CMN_LOG_LVL_ERROR , "rpi_lcmn_fat_fswrite Error 0x%x\n",ret );
		return E_RET_NG_FS_FAT_WRITE;
	}
	/* wait 10s[書き込み待ち時間] */
	udelay (10000000);
	return ret;
}
/* ファイルにあるログをRAMに吐き出す。  */
/*
 */
E_RET_DEFINE rpi_lcmn_log_FromFileToRam( VOID )
{
	UINT_32      ret      = E_RET_OK;
	UINT_8 *     p        = NULL;
	UINT_32      fsize    = 0;
	UINT_32      read_len = 0;
	
	p = (UINT_8 *)gp_buf_log;
	/* OffsetLenの取得[ファイル名からファイルサイズを取得 */
	/* TODO:ファイルGrepしてファイルリネームやログローテート回数の判定を行う処理を今後追加したい */
	ret = rpi_lcmn_FileSizeGet(	D_CMN_LOG_FILE_PATH, 
								D_CMN_DEV_MMC      , 
								D_CMN_DEV_MMC_PART , 
								D_CMN_LOG_FILE_NAME_ONLY ,
								&fsize );
	if( ret != E_RET_OK )
	{
		//ログには残らない[rpi_lcmn_fat_fsreadStrの成功後ログから削除される]
		DBGEP( "rpi_lcmn_FileSizeGet Error 0x%x\n",ret );
		return ret;
	}
	
	ret = rpi_lcmn_fat_fsreadStr( 
				D_CMN_LOG_FILE_TMP_ADDR , /* addr							:システムメモリ内のアドレス       */
				0                       , /* Offset :file_addless postion   :ファイルの相対アドレス位置        */
				(loff_t)fsize           , /* OffsetLen                      :ファイルの相対アドレス位置の長さ  */
				D_CMN_DEV_MMC           , /* dev_name                       :U-bootのデバイス名                */
				D_CMN_DEV_MMC_PART      , /* dev_part_str                   :U-bootのパーツ文字列              */
				D_CMN_LOG_FILE_NAME     , /* filename                       :ファイル名[ディレクトリ付き]      */
				&read_len               ,
		 		p);                       /* 読み出し先の構造体 */
	
	if( ret != 0 )
	{
		D_TP( D_CMN_LOG_LVL_ERROR , "rpi_lcmn_fat_fsreadStr Error 0x%x\n",ret );
		return ret;
	}
	/* テストダンプ,文字列出力と読み出し可能なサイズ */
//	puts((INT_8 *)p);
//	printf("read_len=%d\n",read_len);
	
	return ret;
}
/* ログ初期化処理  */
/*
	@ret : ログファイル->ログRAMへ吐き出し失敗/成功
 */
VOID rpi_lcmn_log_Init( UINT_32 ret )
{
	INT_8 *       p       = NULL;
	INT_8 *      tmp_p    = NULL;
	INT_8 *      tmp_p2   = NULL;
	UINT_32      num_size = 0;
	INT_8        num[32]  = {0};
	ULONG32      index_num= 0;
	ULONG32      index_num_bk= 0;
	ULONG32      cnt      = 0;
	INT_8        output_str[32]={0};
	UINT_32      str_size = 0;
	
	p = (INT_8 *)gp_buf_log;
	DBGEP("[IN]ret=%d\n",ret);
	if( ret != E_RET_OK )
	{
		/* ログテーブルへインデックス0の文字列をコピー */
		memset(p,0x00,DP_BUFLOG_MAX);
		(VOID)strncat( p , D_CMN_LOG_INIT_STR ,strlen(D_CMN_LOG_INIT_STR));
		DBGEP("[PASS]ret=%d\n",ret);
	}
	else
	{
		/* ログRAMないを検索し、最老番のインデックスを見つける */
		/* ログフォーマット前チェック */
		while(( tmp_p = strstr( p ,D_CMN_LOG_FMT_CHK_BEF ) ) != NULL)
		{
			/* インデックスを取得する */
				/* ログフォーマット後チェック */
			//DBGEP("[PASS] tmp_p=%s p=%s \n",tmp_p,p);
			tmp_p2 = strstr( tmp_p ,D_CMN_LOG_FMT_CHK_AFT );
			if( tmp_p2 == NULL )
			{
				/* もしフォーマットが崩れていた場合は強制的にインデックス0の文字列をコピーかつログ全部消去 */
				p = (INT_8 *)gp_buf_log;
				memset(p,0x00,DP_BUFLOG_MAX);
				(VOID)strncat( p , D_CMN_LOG_INIT_STR ,strlen(D_CMN_LOG_INIT_STR));
				DBGEP("[OUT] FMT AFT NULL \n");
				return;
			}
			//DBGEP("[PASS] tmp_p2=%s tmp_p=%s\n",tmp_p2,tmp_p);
			num_size = tmp_p2 - tmp_p;
			//DBGEP("[PASS] num_size=%d tmp_p2=%p tmp_p=%p\n",num_size,tmp_p2,tmp_p);
			if( num_size < 0 )
			{
				/* もし,後のポインタが前のポインタよりアドレスが小さい場合 負の数になるのでポインタ破壊等の異常を考慮する*/
				/* 強制的にインデックス0の文字列をコピーかつログ全部消去 */
				p = (INT_8 *)gp_buf_log;
				memset(p,0x00,DP_BUFLOG_MAX);
				(VOID)strncat( p , D_CMN_LOG_INIT_STR ,strlen(D_CMN_LOG_INIT_STR));
				DBGEP("[OUT] pointer Abnormal\n");
				return;
			}
			/* 初期化 */
			memset(&num ,0x00 ,sizeof(num));

			/* 次のフォーマット位置へ飛ばすために今回のフォーマット位置分ポインタを移動 */
			tmp_p += strlen(D_CMN_LOG_FMT_CHK_BEF);
			//DBGEP("[PASS] tmp_p%s\n",tmp_p);
			strncpy( num , tmp_p , num_size);
			index_num = simple_strtoul( num , NULL, 16);
			if( index_num > index_num_bk )
			{
				DBGEP("[PASS] num=%s index_num=%ld index_num_bk=%ld\n",num , index_num,index_num_bk);
				index_num_bk = index_num;
			}
			/* 次のポインタは<uboot-log:X>のXからに置き換えてからチェックする。 */
			p = tmp_p;
			cnt++;
		}
		if( cnt == 0 )
		{
			/* 文字列中にフォーマット文字が一文字もない場合は強制的にインデックス0の文字列をコピーかつログ全部消去 */
			/* 同名のログファイルだが、壊れている場合などを考慮 */
			p = (INT_8 *)gp_buf_log;
			memset(p,0x00,DP_BUFLOG_MAX);
			(VOID)strncat( p , D_CMN_LOG_INIT_STR ,strlen(D_CMN_LOG_INIT_STR));
			DBGEP("[OUT] FMT AFT NULL \n");
			return;
		}
		
		snprintf(output_str,sizeof(output_str),"%s%ld%s",D_CMN_LOG_FMT_CHK_BEF,++index_num_bk,D_CMN_LOG_FMT_CHK_AFT);
		p = (INT_8 *)gp_buf_log;
		
		/* ファイルの文字サイズ+書き込もうとしているサイズ */
		str_size = strlen(p)+strlen(output_str);
		/* ログRAMの最大容量超過の場合 */
		if( (str_size) >= DP_BUFLOG_MAX )
		{
			/* 初期化して0始まり */
			p = (INT_8 *)gp_buf_log;
			memset(p,0x00,DP_BUFLOG_MAX);
			(VOID)strncat( p , D_CMN_LOG_INIT_STR ,strlen(D_CMN_LOG_INIT_STR));
			DBGEP("[OUT] Log RAM Size Over Init str_size=%d DP_BUFLOG_MAX=%d output_str=%s\n",str_size,DP_BUFLOG_MAX,output_str);
			return;
		}
		
		/* ログテーブルへインデックスの文字列をコピー */
		(VOID)strncat( p , output_str ,strlen(output_str));
	}
	
	/* テストダンプ,文字列出力と読み出し可能なサイズ */
#if 0
	p = (INT_8 *)gp_buf_log;
	puts(p);
	printf("len=%d\n",strlen(D_CMN_LOG_INIT_STR));
#endif
	DBGEP("[OUT]\n");
	return;
}
/* U-boot更新機能 */
E_RET_DEFINE rpi_lcmn_ubootUpdate(VOID)
{
	E_RET_DEFINE ret               = E_RET_OK;
//	INT_8  *dev_name[2]       = {"mmc","usb"};            /* mmc=SDカードかUSBを定義 */
//	INT_8  *dev_part_str[3]   = {"0:1","0:1"};  /* SDカード,USB A */
//	INT_32  fstype[2]         = {FS_TYPE_FAT , FS_TYPE_EXT}; /* FATとEXT4 */
	INT_8   FileName[64]      = {0};
	INT_8   PathFileName[256] = {0};
	INT_8*  PathFileName_p    = NULL;
//	UINT_32 fsize             = 0;
	UINT_32 read_fsize        = 0;
	UINT_8 *f_p               = NULL;
	UINT_32 crc32_calc        = 0;
	S_CMN_HEADER_CONF   LM_FHeader;
	S_CMN_FILE_HEADER   FHeader;
	UINT_8 * addr_p           = NULL;
	UINT_32 nhcnt             = 0;
	
	rpi_lcmn_memset( &LM_FHeader , 0x00 , sizeof(LM_FHeader) );
	rpi_lcmn_memset( &FHeader , 0x00 , sizeof(FHeader) );
	
	/* 読み込み対象を決定する */
	/* SDカード */
	/* TODO:USB A,B */
	
	/* SDカードのFAT面指定 , SDカードは1枚かつパーテションが"0:1"しかないので固定とする */
	/*	以下の*までが本関数の動作。
		0.U-bootの更新チェック&U-bootの書き換え
		0.1.U-bootの更新する場合[U-boot.BINのファイルバージョン情報が不一致]
		0.1.1.U-bootファイル読み出し処理
		0.1.1.1.U-bootファイル読み出し失敗[crc32が不一致,ファイルがない]
		0.1.1.2.現在格納されているU-bootで起動し、1以降の処理を行う
		0.1.2.U-bootファイル読み出しOK
		0.1.3.SDカードへファイルを書き込み
		0.1.3.1.SDカードへファイルを書き込み失敗
		0.1.3.1.1.現在格納されているU-bootで起動し、1以降の処理を行う
		0.1.3.2.SDカードへファイルを書き込み成功
		0.1.3.2.1.U-boot.BINにヘッダを出力,ログ書き込みし、再起動
		0.2.U-bootの更新しない場合
		0.2.1.1の以降の処理を行う。
	 */

	/* U-boot.BINをすべて読み出す */
	ret = rpi_lcmn_fat_fsreadStr( 
			0x01000000    ,
			0             ,
			(loff_t) sizeof(FHeader),
			"mmc"         , 
			"0:1"         , 
			D_CMN_FILE_UBOOTBIN  ,
			&read_fsize   ,
			(UINT_8*)&FHeader );
	if ( ret != E_RET_OK )
	{
		D_TP(D_CMN_LOG_LVL_ERROR,"rpi_lcmn_fat_fsreadStr Read NG. ret=%d actread=%d\n",ret,read_fsize );
		return ret;
	}
	/* バイトオーダ変換 */
	FHeader.crc32      = ntohl(FHeader.crc32      );
	FHeader.offset     = ntohl(FHeader.offset     );
	FHeader.offset_size= ntohl(FHeader.offset_size);
	FHeader.fversion   = ntohs(FHeader.fversion   );

	if( sizeof(FHeader) != read_fsize )
	{
		D_TP(D_CMN_LOG_LVL_ERROR,"rpi_lcmn_fat_fsreadStr ALL Read NG. FHeadersize=%d ActReadSize=%d\n",sizeof(FHeader),read_fsize );
		return E_RET_NG_DB_ACTREADSIZE_NG;
	}
	/* LM NAMEGet処理 */
	ret = rpi_lcmn_FmtFileNameGet(	( INT_8 * )&D_CMN_FILE_LM_FMT_PATH            ,
										( INT_8 * )&D_CMN_FILE_LM_FMT_NAME        ,
										( UINT_32 )strlen(D_CMN_FILE_LM_FMT_NAME) ,
										( INT_8 * )&D_CMN_FILE_LM_FMT_EXT         ,
										( UINT_32 )strlen(D_CMN_FILE_LM_FMT_EXT)  ,
										"mmc",
										"0:1",
										( INT_8 * )&FileName );         
	if( ret != 0 )
	{
		D_TP( D_CMN_LOG_LVL_ERROR , "Error 0x%x\n",ret );
		return ret;
	}
	printf("FileName=%s\n",FileName);

	PathFileName_p = &PathFileName[0];
	strncat(PathFileName_p , D_CMN_FILE_LM_FMT_PATH ,strlen(D_CMN_FILE_LM_FMT_PATH));
	strncat(PathFileName_p , "/" , 1);
	strncat(PathFileName_p , FileName , strlen(FileName));
	printf("PathFileName=%s\n",PathFileName_p);

	/* RPI_LM_FXXXX_YYYY_ZZZZ.LMをヘッダだけ読み出す */
	ret = rpi_lcmn_fat_fsreadStr( 
			0x01000000    ,
			0             ,
			(loff_t) sizeof(LM_FHeader),
			"mmc"         , 
			"0:1"         , 
			PathFileName  ,
			&read_fsize   ,
			(UINT_8*)&LM_FHeader );
	if ( ret != E_RET_OK )
	{
		D_TP(D_CMN_LOG_LVL_ERROR,"rpi_lcmn_fat_fsreadStr Read NG. ret=%d Path=%s actread=%d\n",ret,PathFileName,read_fsize );
		return ret;
	}
	/* バイトオーダ変換 */
	LM_FHeader.crc32       = ntohl(LM_FHeader.crc32      );
    LM_FHeader.maj_version = ntohs(LM_FHeader.maj_version);
	LM_FHeader.min_version = ntohs(LM_FHeader.min_version);
	LM_FHeader.bld_version = ntohs(LM_FHeader.bld_version);
	LM_FHeader.file_size   = ntohl(LM_FHeader.file_size  );
	for( nhcnt = 0 ; nhcnt < ( sizeof(LM_FHeader.file_header)/sizeof(LM_FHeader.file_header[0]) ) ; nhcnt++ )
	{
		LM_FHeader.file_header[nhcnt].crc32      = ntohl(LM_FHeader.file_header[nhcnt].crc32      );
		LM_FHeader.file_header[nhcnt].offset     = ntohl(LM_FHeader.file_header[nhcnt].offset     );
		LM_FHeader.file_header[nhcnt].offset_size= ntohl(LM_FHeader.file_header[nhcnt].offset_size);
		LM_FHeader.file_header[nhcnt].fversion   = ntohs(LM_FHeader.file_header[nhcnt].fversion   );
	}

	/* システムLMヘッダのファイルヘッダ[0]のファイル版数 VersionとU-boot.BINのファイル版数 Versionを比較する */
	if( LM_FHeader.file_header[0].fversion != FHeader.fversion )
	{
		/* 不一致のため、ファイルUpdate */
		/* RPI_LM_FXXXX_YYYY_ZZZZ.LMをu-boot.binのみ読みだす。 */
		ret = rpi_lcmn_fat_fsread( 
				0x01000000 ,
				(loff_t) LM_FHeader.file_header[0].offset,
				(loff_t) LM_FHeader.file_header[0].offset_size,
				"mmc" , 
				"0:1" , 
				PathFileName,
				&read_fsize );
		if ( ret != E_RET_OK )
		{
			D_TP(D_CMN_LOG_LVL_ERROR,"rpi_lcmn_fat_fsread Read NG. ret=%d Path=%s actread=%d\n",ret,PathFileName,read_fsize );
			return ret;
		}
		/* crc32を再計算とヘッダにあるcrc32との一致確認を行う。 */
		make_crc_table();
		f_p = (UINT_8 *)0x01000000;
		crc32_calc  = mycrc32(  f_p , (SIZE_T)(LM_FHeader.file_header[0].offset_size));
		if( crc32_calc != LM_FHeader.file_header[0].crc32 )
		{
			D_TP(D_CMN_LOG_LVL_ERROR,"CRC32 Header NG. crc32 calc =0x%x crc32 file Header=0x%x\n",crc32_calc , LM_FHeader.file_header[0].crc32 );
			return E_RET_NG_DB_CRC32_NG;
		}
		if( read_fsize != LM_FHeader.file_header[0].offset_size )
		{
			D_TP(D_CMN_LOG_LVL_ERROR,"File Size NG.read_fsize=%d offset_size=%d\n",read_fsize , LM_FHeader.file_header[0].offset_size );
			return E_RET_NG_FS_FAT_READ;
		}
		/* u-boot.binへファイル書き込み */
		ret = rpi_lcmn_fat_fswrite( 
			0x01000000 ,
			read_fsize , 
			"mmc" , 
			"0:1" , 
			D_CMN_FILE_UBOOTBIN_NAME );
		
		if( ret != 0 )
		{
			D_TP( D_CMN_LOG_LVL_ERROR , "rpi_lcmn_fat_fswrite u-boot.bin Error 0x%x\n",ret );
			return CMD_RET_FAILURE;
		}
		/* バイトオーダ変換 */
		LM_FHeader.file_header[0].crc32      = htonl(LM_FHeader.file_header[0].crc32      );
		LM_FHeader.file_header[0].offset     = htonl(LM_FHeader.file_header[0].offset     );
		LM_FHeader.file_header[0].offset_size= htonl(LM_FHeader.file_header[0].offset_size);
		LM_FHeader.file_header[0].fversion   = htons(LM_FHeader.file_header[0].fversion   );
		
		/* U-boot.BINへファイル書き込み */
		addr_p = (UINT_8 *)&LM_FHeader.file_header[0];
		ret = rpi_lcmn_fat_fswrite( 
			(ULONG32)addr_p ,
			sizeof(S_CMN_FILE_HEADER) , 
			"mmc" , 
			"0:1" , 
			D_CMN_FILE_UBOOTBIN );
		
		if( ret != 0 )
		{
			D_TP( D_CMN_LOG_LVL_ERROR , "rpi_lcmn_fat_fswrite U-boot.BIN Error 0x%x\n",ret );
			return CMD_RET_FAILURE;
		}
		D_TP( D_CMN_LOG_LVL_INFO , "U-boot Update OK! Version:0x%x\n",htons(LM_FHeader.file_header[0].fversion) );
		/* ログ書き込み */
		rpi_lcmn_log_FromRamToFile("mmc","0:1",D_CMN_LOG_FILE_NAME);
		
		/* U-bootの再起動 */
		puts("...Reset Start...\n");
		do_reset(NULL,0,0,NULL);
		puts("...Reseting...!\n");
	}
	else
	{
		/* 一致のためファイルアップデート不要 */
		D_TP( D_CMN_LOG_LVL_INFO , "U-boot NonUpdate! Version:0x%x\n",LM_FHeader.file_header[0].fversion );
	}
	
	return ret;
}
/* TODO：設定を読み込む */
/* TODO：USB未対応 */
	/* 設定読み込み後ログファイルをRAMに吐き出し追記。 */
E_RET_DEFINE rpi_lcmn_conf_read(VOID)
{
	UINT_32 ret               = E_RET_OK;
//	INT_8  *dev_name[2]       = {"mmc","usb"};            /* mmc=SDカードかUSBを定義 */
//	INT_8  *dev_part_str[3]   = {"0:1","0:1"};  /* SDカード,USB A */
//	INT_32  fstype[2]         = {FS_TYPE_FAT , FS_TYPE_EXT}; /* FATとEXT4 */
	INT_8   FileName[64]      = {0};
	INT_8   PathFileName[256] = {0};
	INT_8*  PathFileName_p    = NULL;
//	UINT_32 fsize             = 0;
	UINT_32 read_fsize        = 0;
	UINT_8 *DB_p              = NULL;
	UINT_32 crc32_calc        = 0;
	S_CMN_DATABASE_HEADER DBHeader;
	
	rpi_lcmn_memset( &DBHeader , 0x00 , sizeof(DBHeader) );
	
	/* 読み込み対象を決定する */
	/* SDカード */
	/* TODO:USB A,B */
	
	/* SDカードのFAT面指定 , SDカードは1枚かつパーテションが"0:1"しかないので固定とする */
	/*	以下の*までが本関数の動作。
		0.U-bootの更新チェック&U-bootの書き換え
		0.1.U-bootの更新する場合[U-boot.BINのVer,Rev,Build情報が不一致]
		0.1.1.U-bootファイル読み出し処理
		0.1.1.1.U-bootファイル読み出し失敗[crc32が不一致,ファイルがない]
		0.1.1.2.現在格納されているU-bootで起動し、1以降の処理を行う
		0.1.2.U-bootファイル読み出しOK
		0.1.3.SDカードへファイルを書き込み
		0.1.3.1.SDカードへファイルを書き込み失敗
		0.1.3.1.1.現在格納されているU-bootで起動し、1以降の処理を行う
		0.1.3.2.SDカードへファイルを書き込み成功
		0.1.3.2.1.U-boot.BINにヘッダを出力,ログ書き込みし、再起動
		0.2.U-bootの更新しない場合
		0.2.1.1の以降の処理を行う。
		*1.データベース読み出し 
		*1.1.データベース読み出し失敗[crc32が不一致、ファイルがない]
		*1.1.1.LM起動とする。
		*1.1.2.LM起動ロード失敗
		*1.1.2.1.起動失敗をログファイルへかき出し、停止。
		*1.1.3.LM起動ロード成功[そのままLM起動シーケンスへ].
		*1.2.データベース読み出し成功時、構造体へデータベース情報を格納する。
		
		1.2.0.データベースからログレベルの定義を取得して設定する。
		1.2.1.TFTP起動か？[TFTPモード/LMモード有無読み込みが1である]
		1.2.1.1.TFTP起動である TFTP起動シーケンスへ
		1.2.2.LM起動である LM起動シーケンスへ[TFTPモード/LMモード有無読み込みが1でない場合]
		2.TFTP起動シーケンスの場合
		2.1.データベースから以下の情報をReadする。
			・TftpServerIP
			・ターゲットIP
			・Linux Kernel Boot Load Addr
			・Linux DTB Boot Load Addr
	        ・Linux RootFS Boot Load Addr
			・Linux Kernel File Name
	        ・Linux DTB File Name
	        ・Linux RootFS FileName 
		2.2. 2.1でReadする情報から以下のコマンド同等の動作を行う
			setenv serverip 192.168.0.150
			setenv ipaddr 192.168.0.10
			tftp 01000000 zImage
			tftp 04000100 bcm2708-rpi-b.dtb
			tftp 02100000 rootfs.cpio.uboot
			rpi_lcmn_LFW mmc 0:1 /bootlog/RPI_UBOOT.LOG
			bootz 01000000 02100000 04000100
			なお、失敗した場合はエラーを出力する。
		3.LM起動シーケンスの場合
		3.1.データベースから以下の情報をReadする。
			・Linux Kernel Boot Load Addr
			・Linux DTB Boot Load Addr
	        ・Linux RootFS Boot Load Addr
		3.2.LMを以下のコマンド相当で読み込むを行う。
			rpi_lcmn_FileRead 01000000 offset[*1] offsetlen[*2] mmc 0:1 /boot/RPI_LM_F_1234_5678_9abc.LM 
			rpi_lcmn_FileRead 02100000 offset[*3] offsetlen[*4] mmc 0:1 /boot/RPI_LM_F_1234_5678_9abc.LM
			rpi_lcmn_FileRead 04000100 offset[*5] offsetlen[*6] mmc 0:1 /boot/RPI_LM_F_1234_5678_9abc.LM
			rpi_lcmn_LFW mmc 0:1 /bootlog/RPI_UBOOT.LOG
			bootz 01000000 02100000 04000100

			*1:Linux Kernel Boot Load addrをLMファイルのヘッダーから読み出し
				読み出し方法
				・2番目のファイルをLinux Kernelのファイルとする。
				・システムLMデータのヘッダーのファイルヘッダーのoffsetとする。
			*2:*1のoffset_sizeを基準とする。
				具体的にはシステムLMデータのヘッダーのファイルヘッダーのoffset_sizeとする。
		    *3:Linux DTB Boot LoadをLMファイルのヘッダーから読み出し
				*1と同様。ただし4番のファイルとする。
			*4:*3のoffset_sizeを基準とする。詳細は*2と同様だが、4番目のファイルとする。
		    *5:Linux RootFS Boot LoadをLMファイルのヘッダーから読み出し
				*1と同様。ただし3番のファイルとする。
			*6:*5のoffset_sizeを基準とする。詳細は*2と同様だが、3番目のファイルとする。	
	 */
	/* データベースファイルNAMEGet処理 */
	ret = rpi_lcmn_FmtFileNameGet(	( INT_8 * )&D_CMN_FILE_CFG_PATH            ,
										( INT_8 * )&D_CMN_FILE_CFG_FMT_NAME        ,
										( UINT_32 )strlen(D_CMN_FILE_CFG_FMT_NAME) ,
										( INT_8 * )&D_CMN_FILE_CFG_FMT_EXT         ,
										( UINT_32 )strlen(D_CMN_FILE_CFG_FMT_EXT)  ,
										"mmc",
										"0:1",
										( INT_8 * )&FileName );         
	if( ret != 0 )
	{
		D_TP( D_CMN_LOG_LVL_ERROR , "Error 0x%x\n",ret );
		return ret;
	}
	printf("FileName=%s\n",FileName);
#if 0	
	ret = rpi_lcmn_FileSizeGet(	( INT_8 * )&D_CMN_FILE_CFG_PATH            ,
									"mmc"    ,
									"0:1"    ,
									FileName ,
									&fsize );
	if( ret != 0 )
	{
		D_TP( D_CMN_LOG_LVL_ERROR , "rpi_lcmn_FileSizeGet Error fsize=%d\n",fsize );
//		rpi_lcmn_log_FromRamToFile( "mmc" , "0:1" ,  D_CMN_LOG_FILE_NAME );
		return CMD_RET_FAILURE;
	}
	printf("fsize=%d\n",fsize);
#endif
	PathFileName_p = &PathFileName[0];
	strncat(PathFileName_p , D_CMN_FILE_CFG_PATH ,strlen(D_CMN_FILE_CFG_PATH));
	strncat(PathFileName_p , "/" ,1);
	strncat(PathFileName_p , FileName , strlen(FileName));
	printf("PathFileName=%s\n",PathFileName_p);
	/* データベースをすべて読み出す */
	ret = rpi_lcmn_fat_fsreadStr( 
			0x01000000    ,
			0             ,
			(loff_t) sizeof(CmnDbAll),
			"mmc"         , 
			"0:1"         , 
			PathFileName_p  ,
			&read_fsize   ,
			(UINT_8*)&CmnDbAll );
	if ( ret != E_RET_OK )
	{
		D_TP(D_CMN_LOG_LVL_ERROR,"rpi_lcmn_fat_fsreadStr Read NG. ret=%d Path=%s actread=%d\n",ret,PathFileName_p,read_fsize );
		return ret;
	}
	if( sizeof(CmnDbAll) != read_fsize )
	{
		D_TP(D_CMN_LOG_LVL_ERROR,"rpi_lcmn_fat_fsreadStr ALL Read NG. DbStrSize=%d ActRead=%d\n",sizeof(CmnDbAll),read_fsize );
		return E_RET_NG_DB_ACTREADSIZE_NG;
	}
	
	/* crc32を再計算とDBのヘッダにあるcrc32との一致確認を行う。 */
	make_crc_table();
	DB_p = (UINT_8*)&CmnDbAll.DBheader.maj_version;/* CRC32を除いた全パラメータから再計算をする。 */
	crc32_calc  = mycrc32( DB_p , (SIZE_T)(sizeof(CmnDbAll)-4));/* CRC32のパラメータを除くのでサイズも全体サイズ-4byteとする */

	/* U-bootのデータベースのバイトオーダ変換 */
		/* 構造体ヘッダー */
	CmnDbAll.DBheader.crc32                = ntohl(CmnDbAll.DBheader.crc32              );
	CmnDbAll.DBheader.maj_version          = ntohs(CmnDbAll.DBheader.maj_version        );
	CmnDbAll.DBheader.min_version          = ntohs(CmnDbAll.DBheader.min_version        );
	CmnDbAll.DBheader.rev_version          = ntohs(CmnDbAll.DBheader.rev_version        );
	CmnDbAll.DBheader.debug_func_addr      = ntohl(CmnDbAll.DBheader.debug_func_addr    );
	CmnDbAll.DBheader.debug_func_addrSize  = ntohl(CmnDbAll.DBheader.debug_func_addrSize);
		/*デバック機能アドレス */
	CmnDbAll.DebugFuncAddr.tftpLmReadOnOff = ntohl(CmnDbAll.DebugFuncAddr.tftpLmReadOnOff );
	CmnDbAll.DebugFuncAddr.ServerIpAddr    = ntohl(CmnDbAll.DebugFuncAddr.ServerIpAddr    );
	CmnDbAll.DebugFuncAddr.tgtIpAddr       = ntohl(CmnDbAll.DebugFuncAddr.tgtIpAddr       );
	CmnDbAll.DebugFuncAddr.kernelLoadAddr  = ntohl(CmnDbAll.DebugFuncAddr.kernelLoadAddr  );
	CmnDbAll.DebugFuncAddr.dtbLoadAddr     = ntohl(CmnDbAll.DebugFuncAddr.dtbLoadAddr     );
	CmnDbAll.DebugFuncAddr.rootfsLoadAddr  = ntohl(CmnDbAll.DebugFuncAddr.rootfsLoadAddr  );
	CmnDbAll.DebugFuncAddr.logLevel        = ntohs(CmnDbAll.DebugFuncAddr.logLevel        );
	
	if( crc32_calc != CmnDbAll.DBheader.crc32 )
	{
		D_TP(D_CMN_LOG_LVL_ERROR,"CRC32 Header NG. crc32 calc=0x%x crc32 file Header=0x%x\n",crc32_calc ,CmnDbAll.DBheader.crc32 );
		return E_RET_NG_DB_CRC32_NG;
	}
	return E_RET_OK;
}
/* コマンド:setenv Start*/
/*
	U-bootのコマンドからコピー
 */
 //env_idは不要。
static int env_id = 1;

static int _do_env_set(int flag, int argc, char * const argv[], int env_flag)
{
	int   i, len;
	char  *name, *value, *s;
	struct env_entry e, *ep;

	debug("Initial value for argc=%d\n", argc);

#if CONFIG_IS_ENABLED(CMD_NVEDIT_EFI)
	if (argc > 1 && argv[1][0] == '-' && argv[1][1] == 'e')
		return do_env_set_efi(NULL, flag, --argc, ++argv);
#endif

	while (argc > 1 && **(argv + 1) == '-') {
		char *arg = *++argv;

		--argc;
		while (*++arg) {
			switch (*arg) {
			case 'f':		/* force */
				env_flag |= H_FORCE;
				break;
			default:
				return CMD_RET_USAGE;
			}
		}
	}
	debug("Final value for argc=%d\n", argc);
	name = argv[1];

	if (strchr(name, '=')) {
		printf("## Error: illegal character '='"
		       "in variable name \"%s\"\n", name);
		return 1;
	}

	env_id++;

	/* Delete only ? */
	if (argc < 3 || argv[2] == NULL) {
		int rc = hdelete_r(name, &env_htab, env_flag);
		return !rc;
	}

	/*
	 * Insert / replace new value
	 */
	for (i = 2, len = 0; i < argc; ++i)
		len += strlen(argv[i]) + 1;

	value = malloc(len);
	if (value == NULL) {
		printf("## Can't malloc %d bytes\n", len);
		return 1;
	}
	for (i = 2, s = value; i < argc; ++i) {
		char *v = argv[i];

		while ((*s++ = *v++) != '\0')
			;
		*(s - 1) = ' ';
	}
	if (s != value)
		*--s = '\0';

	e.key	= name;
	e.data	= value;
	hsearch_r(e, ENV_ENTER, &ep, &env_htab, env_flag);
	free(value);
	if (!ep) {
		printf("## Error inserting \"%s\" variable, errno=%d\n",
			name, errno);
		return 1;
	}

	return 0;
}
static int do_env_set(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	if (argc < 2)
		return CMD_RET_USAGE;

	return _do_env_set(flag, argc, argv, H_INTERACTIVE);
}
//net.c tftpbootはint do_tftpb(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]);がextern宣言されているので本ソースに置かない。

/*
	Input
	UINT_8 * arrayIP      :IPアドレス配列
	size_t arrayIPMax     :IPアドレス配列MAXインデックス
	UINT_32 str_array_cnt :コマンド文字列の配列インデックス
	
	return
	E_RET_DEFINE ret
		E_RET_NG
		E_RET_OK
*/
static E_RET_DEFINE rpi_lcmn_strSetEnvIP( UINT_8 * arrayIP , size_t arrayIPMax ,UINT_32 str_array_cnt )
{
	E_RET_DEFINE ret               = E_RET_OK;
	INT_8        cmd_envset_IP[16] = { 0 };
	UINT_32		 strcnt            = 0;
	INT_32		 ret_cmd           = 0;
	INT_32		 cmd_argc          = 0;
	INT_8 *		 cmd_argv[64]      = { NULL };
	INT_8 *      cmd_p             = NULL;
	
	const INT_8		static_cmd_argv[2][4][64]   = {
		{ "setenv"       ,"serverip",""                    ,"" },
		{ "setenv"       ,"ipaddr"  ,""                    ,"" }
										};
										
	snprintf(cmd_envset_IP,sizeof(cmd_envset_IP),"%03d.%03d.%03d.%03d",*arrayIP,*(arrayIP+1),*(arrayIP+2),*(arrayIP+3));
	for( strcnt = 0 ; strcnt < 3 ; strcnt++ )
	{
		if( strcnt == 2)
		{
			cmd_p = &cmd_envset_IP[0];
		}
		else
		{
			cmd_p = (INT_8*)&static_cmd_argv[str_array_cnt][strcnt][0];
		}
		cmd_argv[strcnt] = cmd_p;
		cmd_argc++;
	}
	/* コマンド:setenv ipaddr 192.168.0.10 */
	ret_cmd = do_env_set(NULL,0,cmd_argc,cmd_argv);
	if( ret_cmd != 0 )
	{
		/* エラーログ出力 */
		D_TP( D_CMN_LOG_LVL_ERROR , "do_env_set Error ret_cmd=%d\n",ret_cmd );
		return E_RET_NG_CMD_SETENV_NG;
	}

	return ret;
}

/*
	Input
	INT_8 * str :文字列の先頭ポインタ
	size_t str_size :文字列のサイズ
	Output
	UINT_32 * ret_cnt : 文字列の先頭からの文字のカウント数[文字以外の数はカウントしない]
	
	return
	E_RET_DEFINE ret
		E_RET_NG
		E_RET_OK
*/
static E_RET_DEFINE rpi_lcmn_str_scope_check( INT_8 * str ,size_t str_size , UINT_32 * ret_cnt )
{
	E_RET_DEFINE ret = E_RET_NG;
	UINT_32 strcnt   = 0;
	INT_8   *str_p   = NULL;
	ret_cnt = 0;
	str_p = str;
	
	/* 文字:SP:0x20～~:0x7Eの場合 */
	while( ( 0x20 <= *str_p )|| ( *str_p <= 0x7E ) )
	{
		if( strcnt >= str_size )
		{
			ret = E_RET_OK;
			break;
		}
		str_p++;
		strcnt++;/* 文字を見つけた数 */
	}
	/* 文字列の末尾がNULLかチェック */
	if( *str_p != '\0' )
	{
		ret = E_RET_NG;
	}
	*ret_cnt = strcnt;
	return ret;
}
/*
	Input
	UINT_32 tftp_boot_addr                 :TFTP boot アドレス
	INT_8        cmd_tftpboot_FileName     :TFTPコマンドで使用するファイル名
	INT_8        cmd_tftpboot_FileNameSize :TFTPコマンドで使用するファイル名のサイズ
	UINT_32 str_array_cnt                  :コマンド文字列の配列インデックス
	
	return
	E_RET_DEFINE ret
		E_RET_NG
		E_RET_OK
*/
static E_RET_DEFINE rpi_lcmn_strTftpBoot( UINT_32 tftp_boot_addr , INT_8 *cmd_tftpboot_FileName , size_t cmd_tftpboot_FileNameSize ,UINT_32 str_array_cnt )
{
	E_RET_DEFINE ret                          = E_RET_OK;
	E_RET_DEFINE ret_internal                 = E_RET_OK;
	UINT_32      strcnt                       = 0;
	INT_8 *         cmd_p                     = NULL;
	INT_8        cmd_tftpboot_Addr[9]         = {0};
	INT_8 *      FileName_p                   = NULL;
	const INT_8		static_cmd_argv[3][3][64] = {
		{ "tftpboot"     ,""        ,"zImage"              },
		{ "tftpboot"     ,""        ,"bcm2708-rpi-b.dtb"   },
		{ "tftpboot"     ,""        ,"rootfs.cpio.uboot"   }
										};
	INT_32			cmd_argc                  = 0;
	INT_8 *			cmd_argv[64]              = { NULL };
	INT_32			ret_cmd                   = 0;
	UINT_32			nullcnt                   = 0;
	
	FileName_p = cmd_tftpboot_FileName;
	//文字列作成
	snprintf(cmd_tftpboot_Addr,sizeof(cmd_tftpboot_Addr),"%08x",tftp_boot_addr);
	for( strcnt = 0 ; strcnt < 3 ; strcnt++ )
	{
		if( strcnt == 1 )
		{
			cmd_p = &cmd_tftpboot_Addr[0];
		}
		else if( strcnt == 2 )
		{
			/* 文字の範囲チェック */
			ret_internal = rpi_lcmn_str_scope_check( FileName_p ,cmd_tftpboot_FileNameSize , &nullcnt );
			if( ret_internal != E_RET_OK )
			{
				/* エラーログ出力 */
				D_TP( D_CMN_LOG_LVL_ERROR , "rpi_lcmn_str_scope_check Error 0x%x ret_cnt=%d\n",ret_internal , nullcnt );
			}
			if( ( ret_internal == E_RET_OK )&&( !( nullcnt == 0 ) ) )
			{
				/* 文字の場合 */
				cmd_p = FileName_p;
			}
			else
			{
				/* 文字じゃない場合,デフォルトのファイル名を使用する */
				cmd_p = (INT_8*)&static_cmd_argv[str_array_cnt][strcnt][0];
			}
		}
		else
		{
			cmd_p = (INT_8*)&static_cmd_argv[str_array_cnt][strcnt][0];
		}
		cmd_argv[strcnt] = cmd_p;
		cmd_argc++;
	}
	/* コマンド:tftpboot アドレス ファイル名 */
	ret_cmd = do_tftpb(NULL,0,cmd_argc,cmd_argv);
	if( ret_cmd != 0 )
	{
		/* エラーログ出力 */
		D_TP( D_CMN_LOG_LVL_ERROR , "do_tftpb Error ret_cmd=%d\n",ret_cmd );
		return E_RET_NG_CMD_TFTPBOOT_NG;
	}

	return ret;
}
/* コマンド:setenv End */
/* TFTP起動 */
E_RET_DEFINE rpi_lcmn_TftpBoot(VOID)
{
	E_RET_DEFINE	ret                    = E_RET_OK;
	INT_32			ret_cmd                = 0;
	INT_32			cmd_argc               = 0;
	INT_8 *			cmd_argv[64]           = { NULL };
	const INT_8		static_cmd_argv[4][64] = {"bootz"        ,""        ,""                    ,"" };
	INT_8           cmd_envset_IP[16]      = { 0 };
	UINT_8			ServerIP[4]            = { 0 };
	UINT_8			targetIP[4]            = { 0 };
	UINT_32			strcnt                 = 0;
	UINT_32			tftp_boot_addr         = 0;
	INT_8        cmd_tftpboot_FileName[64] = {0};
	INT_8        cmd_tftpboot_Addr[3][9]   = {{0}};
	INT_8 *         cmd_p                  = NULL;
	/* 読み込み対象を決定する */
	/* SDカード */
	/* TODO:USB A,B */
	
	/* SDカードのFAT面指定 , SDカードは1枚かつパーテションが"0:1"しかないので固定とする */
	/*	以下の*までが本関数の動作。
		2.TFTP起動シーケンスの場合
		2.1.データベースから以下の情報をReadする。
			・TftpServerIP
			・ターゲットIP
			・Linux Kernel Boot Load Addr
			・Linux DTB Boot Load Addr
	        ・Linux RootFS Boot Load Addr
			・Linux Kernel File Name
	        ・Linux DTB File Name
	        ・Linux RootFS FileName 
		2.2. 2.1でReadする情報から以下のコマンド同等の動作を行う
			setenv serverip 192.168.0.150
			setenv ipaddr 192.168.0.10
			tftpboot 01000000 zImage
			tftpboot 04000100 bcm2708-rpi-b.dtb
			tftpboot 02100000 rootfs.cpio.uboot
			rpi_lcmn_LFW mmc 0:1 /bootlog/RPI_UBOOT.LOG
			bootz 01000000 02100000 04000100
			なお、失敗した場合はエラーを出力する。
	 */
	//TftpServerIPを取得
	ServerIP[0] = ( CmnDbAll.DebugFuncAddr.ServerIpAddr & 0xFF000000 )>>24;
	ServerIP[1] = ( CmnDbAll.DebugFuncAddr.ServerIpAddr & 0x00FF0000 )>>16;
	ServerIP[2] = ( CmnDbAll.DebugFuncAddr.ServerIpAddr & 0x0000FF00 )>>8;
	ServerIP[3] = ( CmnDbAll.DebugFuncAddr.ServerIpAddr & 0x000000FF );
	/* コマンド:setenv serverip 192.168.0.150 */
	ret = rpi_lcmn_strSetEnvIP( ServerIP , (size_t)(sizeof(ServerIP)/sizeof(ServerIP[0])) , 0 );
	if( ret != E_RET_OK )
	{
		/* エラーログ出力 */
		D_TP( D_CMN_LOG_LVL_ERROR , "rpi_lcmn_strSetEnvIP Error ret=%d ServerIPAddr=%03d.%03d.%03d.%03d \n", ret , ServerIP[0],ServerIP[1],ServerIP[2],ServerIP[3] );
		return ret;
	}

	/* 初期化 */
	strcnt = 0;
	/* cmd_argv[strcnt]をNULL初期化 */
	while( cmd_argv[strcnt] != NULL ){	cmd_argv[strcnt] = NULL;	strcnt++;	}
	/* cmd_envset_IPを0初期化 */
	memset( &cmd_envset_IP , 0x00 ,sizeof(cmd_envset_IP) );
	/* cmd_argc初期化 */
	cmd_argc = 0;

	//ターゲットIP取得
	targetIP[0] = ( CmnDbAll.DebugFuncAddr.tgtIpAddr & 0xFF000000 )>>24;
	targetIP[1] = ( CmnDbAll.DebugFuncAddr.tgtIpAddr & 0x00FF0000 )>>16;
	targetIP[2] = ( CmnDbAll.DebugFuncAddr.tgtIpAddr & 0x0000FF00 )>>8;
	targetIP[3] = ( CmnDbAll.DebugFuncAddr.tgtIpAddr & 0x000000FF );
	/* コマンド:setenv ipaddr 192.168.0.10 */
	ret = rpi_lcmn_strSetEnvIP( targetIP , (size_t)(sizeof(targetIP)/sizeof(targetIP[0])) , 1 );
	if( ret != E_RET_OK )
	{
		/* エラーログ出力 */
		D_TP( D_CMN_LOG_LVL_ERROR , "rpi_lcmn_strSetEnvIP Error ret=%d TgtIPaddr=%03d.%03d.%03d.%03d \n", ret , targetIP[0],targetIP[1],targetIP[2],targetIP[3] );
		return ret;
	}
	/* 初期化 */
	strcnt = 0;
	/* cmd_argv[strcnt]をNULL初期化 */
	while( cmd_argv[strcnt] != NULL ){	cmd_argv[strcnt] = NULL;	strcnt++;	}
	/* cmd_argc初期化 */
	cmd_argc = 0;
	
	/* Linux Kernel Boot Load Addr取得 */
	tftp_boot_addr = CmnDbAll.DebugFuncAddr.kernelLoadAddr;
	/* Linux Kernel File Name取得 */
	strncpy( cmd_tftpboot_FileName , CmnDbAll.DebugFuncAddr.tftpKernelFileName , strlen(cmd_tftpboot_FileName));
	
	//文字列作成
	//コマンド:tftpboot 01000000 zImage文字列作成
	ret = rpi_lcmn_strTftpBoot( tftp_boot_addr , cmd_tftpboot_FileName , (sizeof(cmd_tftpboot_FileName)/sizeof(cmd_tftpboot_FileName[0])) ,0 );
	if( ret != E_RET_OK )
	{
		/* エラーログ出力 */
		D_TP( D_CMN_LOG_LVL_ERROR , "rpi_lcmn_strTftpBoot Error ret=%d Addr=0x%x KernalFName=%s\n",ret , tftp_boot_addr, cmd_tftpboot_FileName );
		return ret;
	}
	
	/* 初期化 */
	strcnt = 0;
	/* cmd_argv[strcnt]をNULL初期化 */
	while( cmd_argv[strcnt] != NULL ){	cmd_argv[strcnt] = NULL;	strcnt++;	}
	/* cmd_argc初期化 */
	cmd_argc = 0;
	/* cmd_tftpboot_FileNameの初期化 */
	memset(&cmd_tftpboot_FileName,0x00,sizeof(cmd_tftpboot_FileName));
	
	/* Linux DTB Boot Load Addr取得 */
	tftp_boot_addr = CmnDbAll.DebugFuncAddr.dtbLoadAddr;
	/* Linux DTB File Name取得 */
	strncpy( cmd_tftpboot_FileName , CmnDbAll.DebugFuncAddr.tftpDtbFileName , strlen(cmd_tftpboot_FileName));
	
	//コマンド:tftpboot 04000100 bcm2708-rpi-b.dtb文字列作成
	ret = rpi_lcmn_strTftpBoot( tftp_boot_addr , cmd_tftpboot_FileName , (sizeof(cmd_tftpboot_FileName)/sizeof(cmd_tftpboot_FileName[0])) , 1 );
	if( ret != E_RET_OK )
	{
		/* エラーログ出力 */
		D_TP( D_CMN_LOG_LVL_ERROR , "rpi_lcmn_strTftpBoot Error ret=%d Addr=0x%x DeviceTreeFName=%s\n",ret , tftp_boot_addr, cmd_tftpboot_FileName );
		return ret;
	}
	
	/* 初期化 */
	strcnt = 0;
	/* cmd_argv[strcnt]をNULL初期化 */
	while( cmd_argv[strcnt] != NULL ){	cmd_argv[strcnt] = NULL;	strcnt++;	}
	/* cmd_argc初期化 */
	cmd_argc = 0;
	/* cmd_tftpboot_FileNameの初期化 */
	memset(&cmd_tftpboot_FileName,0x00,sizeof(cmd_tftpboot_FileName));
	
	/* Linux RootFS Boot Load Addr取得 */
	tftp_boot_addr = CmnDbAll.DebugFuncAddr.rootfsLoadAddr;
	/* Linux RootFS FileName取得 */
	strncpy( cmd_tftpboot_FileName , CmnDbAll.DebugFuncAddr.tftprootfsFileName , strlen(cmd_tftpboot_FileName));
	
	//コマンド:tftpboot 02100000 rootfs.cpio.uboot文字列作成
	ret = rpi_lcmn_strTftpBoot( tftp_boot_addr , cmd_tftpboot_FileName , (sizeof(cmd_tftpboot_FileName)/sizeof(cmd_tftpboot_FileName[0])) , 2 );
	if( ret != E_RET_OK )
	{
		/* エラーログ出力 */
		D_TP( D_CMN_LOG_LVL_ERROR , "rpi_lcmn_strTftpBoot Error ret=%d Addr=0x%x rootfsFName=%s\n",ret , tftp_boot_addr, cmd_tftpboot_FileName );
		return ret;
	}
	
	/* 初期化 */
	strcnt = 0;
	/* cmd_argv[strcnt]をNULL初期化 */
	while( cmd_argv[strcnt] != NULL ){	cmd_argv[strcnt] = NULL;	strcnt++;	}
	/* cmd_argc初期化 */
	cmd_argc = 0;
	
	//文字列作成
	snprintf(cmd_tftpboot_Addr[0],sizeof(cmd_tftpboot_Addr[0]),"%08x",CmnDbAll.DebugFuncAddr.kernelLoadAddr);
	snprintf(cmd_tftpboot_Addr[1],sizeof(cmd_tftpboot_Addr[1]),"%08x",CmnDbAll.DebugFuncAddr.rootfsLoadAddr);
	snprintf(cmd_tftpboot_Addr[2],sizeof(cmd_tftpboot_Addr[2]),"%08x",CmnDbAll.DebugFuncAddr.dtbLoadAddr);
	for( strcnt = 0 ; strcnt < 4 ; strcnt++ )
	{
		if( strcnt == 0 )
		{
			cmd_p = (INT_8*)&static_cmd_argv[strcnt][0];
		}
		else
		{
			cmd_p = (INT_8*)&cmd_tftpboot_Addr[strcnt-1][0];
		}
		cmd_argv[strcnt] = cmd_p;
		cmd_argc++;
	}
	
	D_TP( D_CMN_LOG_LVL_INFO , "cmd_argc=%d Bootcmd:%s %s %s %s\n", cmd_argc , cmd_argv[0] , cmd_argv[1] , cmd_argv[2] , cmd_argv[3] );
	/* ログ書き込み */
	rpi_lcmn_log_FromRamToFile("mmc","0:1",D_CMN_LOG_FILE_NAME);
	
	/* コマンド:bootz 01000000 02100000 04000100 */
	ret_cmd = do_bootz(NULL,0,cmd_argc,cmd_argv);
	if( ret_cmd != 0 )
	{
		/* エラーログ出力 */
		D_TP( D_CMN_LOG_LVL_ERROR , "do_bootz Error ret_cmd=%d bootz %s %s %s\n",ret_cmd,cmd_tftpboot_Addr[0],cmd_tftpboot_Addr[1],cmd_tftpboot_Addr[2] );
		return E_RET_NG_CMD_TFTPBOOT_NG;
	}
	/* bootz成功時はここに戻ってこない。 */
	return ret;
}
/* LM起動 */
E_RET_DEFINE rpi_lcmn_LMBoot(VOID)
{
	E_RET_DEFINE	ret                    = E_RET_OK;
	INT_8   FileName[64]                   = {0};
	INT_8   PathFileName[256]              = {0};
	INT_8*  PathFileName_p                 = NULL;
	UINT_32 read_fsize[4]                  = {0};
	S_CMN_HEADER_CONF   LM_FHeader;
	UINT_32 crc32_calc                     = 0;
	INT_32			ret_cmd                = 0;
	INT_32			cmd_argc               = 0;
	INT_8 *			cmd_argv[64]           = { NULL };
	const INT_8		static_cmd_argv[4][64] = {"bootz"        ,""        ,""                    ,"" };
	UINT_32			strcnt                 = 0;
	INT_8           cmd_lmboot_Addr[3][9]  = {{0}};
	INT_8 *         cmd_p                  = NULL;
	UINT_32			nhcnt                  = 0;
	/*
		3.1.データベースから以下の情報をReadする。
			・Linux Kernel Boot Load Addr
			・Linux DTB Boot Load Addr
		    ・Linux RootFS Boot Load Addr
		3.2.LMを以下のコマンド相当で読み込むを行う。
			kernel読み込み
			rpi_lcmn_FileRead 01000000 offset[*1] offsetlen[*2] mmc 0:1 /boot/RPI_LM_F_1234_5678_9abc.LM 
			Device Tree
			rpi_lcmn_FileRead 02100000 offset[*3] offsetlen[*4] mmc 0:1 /boot/RPI_LM_F_1234_5678_9abc.LM
			rootfs
			rpi_lcmn_FileRead 04000100 offset[*5] offsetlen[*6] mmc 0:1 /boot/RPI_LM_F_1234_5678_9abc.LM
			rpi_lcmn_LFW mmc 0:1 /bootlog/RPI_UBOOT.LOG
			bootz 01000000 02100000 04000100

			*1:Linux Kernel Boot Load addrをLMファイルのヘッダーから読み出し
				読み出し方法
				・2番目のファイルをLinux Kernelのファイルとする。
				・システムLMデータのヘッダーのファイルヘッダーのoffsetとする。
			*2:*1のoffset_sizeを基準とする。
				具体的にはシステムLMデータのヘッダーのファイルヘッダーのoffset_sizeとする。
		    *3:Linux DTB Boot LoadをLMファイルのヘッダーから読み出し
				*1と同様。ただし4番のファイルとする。
			*4:*3のoffset_sizeを基準とする。詳細は*2と同様だが、4番目のファイルとする。
		    *5:Linux RootFS Boot LoadをLMファイルのヘッダーから読み出し
				*1と同様。ただし3番のファイルとする。
			*6:*5のoffset_sizeを基準とする。詳細は*2と同様だが、3番目のファイルとする。
	*/
	
	rpi_lcmn_memset( &LM_FHeader , 0x00 , sizeof(LM_FHeader) );
	
	/* LM NAMEGet処理 */
	ret = rpi_lcmn_FmtFileNameGet(	( INT_8 * )&D_CMN_FILE_LM_FMT_PATH            ,
										( INT_8 * )&D_CMN_FILE_LM_FMT_NAME        ,
										( UINT_32 )strlen(D_CMN_FILE_LM_FMT_NAME) ,
										( INT_8 * )&D_CMN_FILE_LM_FMT_EXT         ,
										( UINT_32 )strlen(D_CMN_FILE_LM_FMT_EXT)  ,
										"mmc",
										"0:1",
										( INT_8 * )&FileName );         
	if( ret != 0 )
	{
		D_TP( D_CMN_LOG_LVL_ERROR , "Error 0x%x\n",ret );
		return ret;
	}
	printf("FileName=%s\n",FileName);

	PathFileName_p = &PathFileName[0];
	strncat(PathFileName_p , D_CMN_FILE_LM_FMT_PATH ,strlen(D_CMN_FILE_LM_FMT_PATH));
	strncat(PathFileName_p , "/" , 1);
	strncat(PathFileName_p , FileName , strlen(FileName));
	printf("PathFileName=%s\n",PathFileName_p);

	/* RPI_LM_FXXXX_YYYY_ZZZZ.LMをヘッダだけ読み出す */
	ret = rpi_lcmn_fat_fsreadStr( 
			0x01000000    ,
			0             ,
			(loff_t) sizeof(LM_FHeader),
			"mmc"         , 
			"0:1"         , 
			PathFileName  ,
			&read_fsize[0],
			(UINT_8*)&LM_FHeader );
	if ( ret != E_RET_OK )
	{
		D_TP(D_CMN_LOG_LVL_ERROR,"rpi_lcmn_fat_fsreadStr Read NG. RPI_LM_FXXXX_YYYY_ZZZZ.LM Header ret=%d FOffsetSize=%d Path=%s actread=%d\n",
			ret,
			sizeof(LM_FHeader),
			PathFileName,
			read_fsize[0] );
		return ret;
	}
	/* バイトオーダ変換 */
	LM_FHeader.crc32       = ntohl(LM_FHeader.crc32      );
    LM_FHeader.maj_version = ntohs(LM_FHeader.maj_version);
	LM_FHeader.min_version = ntohs(LM_FHeader.min_version);
	LM_FHeader.bld_version = ntohs(LM_FHeader.bld_version);
	LM_FHeader.file_size   = ntohl(LM_FHeader.file_size  );
	for( nhcnt = 0 ; nhcnt < ( sizeof(LM_FHeader.file_header)/sizeof(LM_FHeader.file_header[0]) ) ; nhcnt++ )
	{
		LM_FHeader.file_header[nhcnt].crc32      = ntohl(LM_FHeader.file_header[nhcnt].crc32      );
		LM_FHeader.file_header[nhcnt].offset     = ntohl(LM_FHeader.file_header[nhcnt].offset     );
		LM_FHeader.file_header[nhcnt].offset_size= ntohl(LM_FHeader.file_header[nhcnt].offset_size);
		LM_FHeader.file_header[nhcnt].fversion   = ntohs(LM_FHeader.file_header[nhcnt].fversion   );
	}
	/* kernel読み込み */
	ret = rpi_lcmn_fat_fsread( 
				CmnDbAll.DebugFuncAddr.kernelLoadAddr ,/* addr							 :システムメモリ内のアドレス        */
				LM_FHeader.file_header[1].offset      ,/* Offset :file_addless postion   :ファイルの相対アドレス位置        */
				LM_FHeader.file_header[1].offset_size ,/* OffsetLen                      :ファイルの相対アドレス位置の長さ  */
				"mmc"                                 ,/* dev_name                       :U-bootのデバイス名                */
				"0:1"                                 ,/* dev_part_str                   :U-bootのパーツ文字列              */
				PathFileName                          ,/* filename                       :ファイル名[ディレクトリ付き]      */
				&read_fsize[1] );         
	if( ret != E_RET_OK )
	{
		D_TP(D_CMN_LOG_LVL_ERROR,"rpi_lcmn_fat_fsreadStr Read NG. RPI_LM_FXXXX_YYYY_ZZZZ.LM Kernel ret=%d MemAddr=0x%x FOffset=0x%x FOffsetSize=%d Path=%s actread=%d\n",
			ret,
			CmnDbAll.DebugFuncAddr.kernelLoadAddr,
			LM_FHeader.file_header[1].offset,
			LM_FHeader.file_header[1].offset_size,
			PathFileName,
			read_fsize[1] );
		return ret;
	}
	/* Kernelファイルのチェック crc32を再計算とLMのファイルヘッダにあるcrc32との一致確認を行う。 */
	make_crc_table();
	crc32_calc  = mycrc32( (UINT_8 *)CmnDbAll.DebugFuncAddr.kernelLoadAddr , (SIZE_T)(LM_FHeader.file_header[1].offset_size));
	if( crc32_calc != LM_FHeader.file_header[1].crc32 )
	{
		D_TP(D_CMN_LOG_LVL_ERROR,"Kernel CRC32 Header NG. crc32 calc =0x%x crc32 file Header=0x%x\n",crc32_calc , LM_FHeader.file_header[1].crc32 );
		return E_RET_NG_DB_CRC32_NG;
	}

	/* Device Tree読み込み */
	ret = rpi_lcmn_fat_fsread( 
				CmnDbAll.DebugFuncAddr.dtbLoadAddr    ,/* addr							 :システムメモリ内のアドレス        */
				LM_FHeader.file_header[2].offset      ,/* Offset :file_addless postion   :ファイルの相対アドレス位置        */
				LM_FHeader.file_header[2].offset_size ,/* OffsetLen                      :ファイルの相対アドレス位置の長さ  */
				"mmc"                                 ,/* dev_name                       :U-bootのデバイス名                */
				"0:1"                                 ,/* dev_part_str                   :U-bootのパーツ文字列              */
				PathFileName                          ,/* filename                       :ファイル名[ディレクトリ付き]      */
				&read_fsize[2] );         
	if( ret != E_RET_OK )
	{
		D_TP(D_CMN_LOG_LVL_ERROR,"rpi_lcmn_fat_fsreadStr Read NG. RPI_LM_FXXXX_YYYY_ZZZZ.LM DeviceTree ret=%d MemAddr=0x%x FOffset=0x%x FOffsetSize=%d Path=%s actread=%d\n",
			ret,
			CmnDbAll.DebugFuncAddr.dtbLoadAddr,
			LM_FHeader.file_header[2].offset,
			LM_FHeader.file_header[2].offset_size,
			PathFileName,
			read_fsize[2] );
		return ret;
	}
	/* Device Treeファイルのチェック crc32を再計算とLMのファイルヘッダにあるcrc32との一致確認を行う。 */
	make_crc_table();
	crc32_calc  = mycrc32( (UINT_8 *)CmnDbAll.DebugFuncAddr.dtbLoadAddr , (SIZE_T)(LM_FHeader.file_header[2].offset_size));
	if( crc32_calc != LM_FHeader.file_header[2].crc32 )
	{
		D_TP(D_CMN_LOG_LVL_ERROR,"DeviceTree CRC32 Header NG. crc32 calc =0x%x crc32 file Header=0x%x\n",crc32_calc , LM_FHeader.file_header[2].crc32 );
		return E_RET_NG_DB_CRC32_NG;
	}
	/* rootfs読み込み */
	ret = rpi_lcmn_fat_fsread( 
				CmnDbAll.DebugFuncAddr.rootfsLoadAddr ,/* addr							 :システムメモリ内のアドレス        */
				LM_FHeader.file_header[3].offset      ,/* Offset :file_addless postion   :ファイルの相対アドレス位置        */
				LM_FHeader.file_header[3].offset_size ,/* OffsetLen                      :ファイルの相対アドレス位置の長さ  */
				"mmc"                                 ,/* dev_name                       :U-bootのデバイス名                */
				"0:1"                                 ,/* dev_part_str                   :U-bootのパーツ文字列              */
				PathFileName                          ,/* filename                       :ファイル名[ディレクトリ付き]      */
				&read_fsize[3] );         
	if( ret != E_RET_OK )
	{
		D_TP(D_CMN_LOG_LVL_ERROR,"rpi_lcmn_fat_fsreadStr Read NG. RPI_LM_FXXXX_YYYY_ZZZZ.LM rootfs ret=%d MemAddr=0x%x FOffset=0x%x FOffsetSize=%d Path=%s actread=%d\n",
			ret,
			CmnDbAll.DebugFuncAddr.rootfsLoadAddr,
			LM_FHeader.file_header[3].offset,
			LM_FHeader.file_header[3].offset_size,
			PathFileName,
			read_fsize[3] );
		return ret;
	}
	/* rootfsファイルのチェック crc32を再計算とLMのファイルヘッダにあるcrc32との一致確認を行う。 */
	make_crc_table();
	crc32_calc  = mycrc32( (UINT_8 *)CmnDbAll.DebugFuncAddr.rootfsLoadAddr , (SIZE_T)(LM_FHeader.file_header[3].offset_size));
	if( crc32_calc != LM_FHeader.file_header[3].crc32 )
	{
		D_TP(D_CMN_LOG_LVL_ERROR,"rootfs CRC32 Header NG. crc32 calc =0x%x crc32 file Header=0x%x\n",crc32_calc , LM_FHeader.file_header[3].crc32 );
		return E_RET_NG_DB_CRC32_NG;
	}
	/* 読み出し成功時の情報出力 kernel,devicetree,rootfs */
	D_TP(D_CMN_LOG_LVL_INFO,"RPI_LM_FXXXX_YYYY_ZZZZ.LM Path=%s kernel/DT/rootfs MemAddr=%x/%x/%x FOffset=%x/%x/%x FOffsetSize=%d/%d/%d actread=%d/%d/%d\n",
		PathFileName                         ,
		CmnDbAll.DebugFuncAddr.kernelLoadAddr,
		CmnDbAll.DebugFuncAddr.dtbLoadAddr   ,
		CmnDbAll.DebugFuncAddr.rootfsLoadAddr,
		LM_FHeader.file_header[1].offset     ,
		LM_FHeader.file_header[2].offset     ,
		LM_FHeader.file_header[3].offset     ,
		LM_FHeader.file_header[1].offset_size,
		LM_FHeader.file_header[2].offset_size,
		LM_FHeader.file_header[3].offset_size,
		read_fsize[1]                        ,
		read_fsize[2]                        ,
		read_fsize[3]
	);
	//文字列作成
	snprintf(cmd_lmboot_Addr[0],sizeof(cmd_lmboot_Addr[0]),"%08x",CmnDbAll.DebugFuncAddr.kernelLoadAddr);
	snprintf(cmd_lmboot_Addr[1],sizeof(cmd_lmboot_Addr[1]),"%08x",CmnDbAll.DebugFuncAddr.rootfsLoadAddr);
	snprintf(cmd_lmboot_Addr[2],sizeof(cmd_lmboot_Addr[2]),"%08x",CmnDbAll.DebugFuncAddr.dtbLoadAddr);
	for( strcnt = 0 ; strcnt < 4 ; strcnt++ )
	{
		if( strcnt == 0 )
		{
			cmd_p = (INT_8*)&static_cmd_argv[strcnt][0];
		}
		else
		{
			cmd_p = (INT_8*)&cmd_lmboot_Addr[strcnt-1][0];
		}
		cmd_argv[strcnt] = cmd_p;
		cmd_argc++;
	}
	
	D_TP( D_CMN_LOG_LVL_INFO , "cmd_argc=%d Bootcmd:%s %s %s %s\n", cmd_argc , cmd_argv[0] , cmd_argv[1] , cmd_argv[2] , cmd_argv[3] );
	/* ログ書き込み */
	rpi_lcmn_log_FromRamToFile("mmc","0:1",D_CMN_LOG_FILE_NAME);
	
	/* コマンド:bootz 01000000 02100000 04000100 */
	ret_cmd = do_bootz(NULL,0,cmd_argc,cmd_argv);
	if( ret_cmd != 0 )
	{
		/* エラーログ出力 */
		D_TP( D_CMN_LOG_LVL_ERROR , "do_bootz Error ret_cmd=%d bootz %s %s %s\n",ret_cmd,cmd_lmboot_Addr[0],cmd_lmboot_Addr[1],cmd_lmboot_Addr[2] );
		return E_RET_NG_CMD_LMBOOT_NG;
	}
	/* bootz成功時はここに戻ってこない。 */
	return ret;
}
/* 初期化処理[本関数を使用する前に初期化する][TODO:呼び出し先の選定] */
VOID rpi_lcmn_init(VOID)
{
	E_RET_DEFINE ret = 0;
	
	/* ログレベル変数 */
	cmn_printf_loglvl_set_val = D_CMN_LOG_LVL_DEBUG;/* DEBUG設定:DB読み込みまではLevelは初期設定のまま */
	/* グローバルバッファログ0初期化 */
	memset(&gp_buf_log,0x00,sizeof(gp_buf_log));
	memset(&gp_buf_log_tmp,0x00,sizeof(gp_buf_log_tmp));
	memset(&CmnDbAll,0x00,sizeof(CmnDbAll));
	
	DBGEP("Init Start...\n");
	
	/* ログファイル->RAMへログをコピー */
	ret = rpi_lcmn_log_FromFileToRam();
	
	/* ログを初期化する */
	rpi_lcmn_log_Init( ret );
	
	/* U-boot更新機能 */
	ret = rpi_lcmn_ubootUpdate();
	if( ret != E_RET_OK )
	{
		D_TP( D_CMN_LOG_LVL_ERROR , "U-boot Update Error 0x%x\n",ret );
		goto GO_RETURN;
	}
	/* DB読み取り */
	ret = rpi_lcmn_conf_read();
	if( ret != E_RET_OK )
	{
		D_TP( D_CMN_LOG_LVL_ERROR , "Error 0x%x\n",ret );
		goto GO_RETURN;
	}
	else
	{
		/* 読み取りOKである場合 */
		/* ログレベル変数をDBから設定 */
		ret = rpi_lcmn_lvl_chg(CmnDbAll.DebugFuncAddr.logLevel);
		if( ret != E_RET_OK )
		{
			D_TP( D_CMN_LOG_LVL_ERROR , "DB Set Error 0x%x\n",ret );
			goto GO_RETURN;
		}
		/* TFTP/LM boot起動機能 */
	}
	
	/*
		1.2.1.TFTP起動か？[TFTPモード/LMモード有無読み込みが1である]
		1.2.1.1.TFTP起動である TFTP起動シーケンスへ
		1.2.2.LM起動である LM起動シーケンスへ[TFTPモード/LMモード有無読み込みが1でない場合]
	*/
	if( ( CmnDbAll.DebugFuncAddr.tftpLmReadOnOff & D_CMN_DB_DEBUG_TFTPLMREADONOFF_ON ) == D_CMN_DB_DEBUG_TFTPLMREADONOFF_ON  )
	{
		/* TFTP起動 */
		ret = rpi_lcmn_TftpBoot();
		if( ret != E_RET_OK )
		{
			D_TP( D_CMN_LOG_LVL_ERROR , "rpi_lcmn_TftpBoot Error 0x%x\n",ret );
			goto GO_RETURN;
		}
	}
	else
	{
		/* LM起動 */
		ret = rpi_lcmn_LMBoot();
		if( ret != E_RET_OK )
		{
			D_TP( D_CMN_LOG_LVL_ERROR , "rpi_lcmn_LMBoot Error 0x%x\n",ret );
			goto GO_RETURN;
		}
	}
	
	/* 初期化完了済みを設定 */
	gp_init_flg = D_CMN_ON;

GO_RETURN:
	/* 失敗がなければログ保存はされない。*/
	D_TP( D_CMN_LOG_LVL_FATAL , "gp_init_flg %d\n",gp_init_flg );
	/* NGの場合はログ保存されていないのでログ保存する */
	if( ret != E_RET_OK )
	{
		/* ログ書き込み */
		rpi_lcmn_log_FromRamToFile("mmc","0:1",D_CMN_LOG_FILE_NAME);
	}
	DBGEP("Init End OK\n");
}
